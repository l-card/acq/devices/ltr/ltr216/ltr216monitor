cmake_minimum_required(VERSION 2.8.11)

project(LTR216Monitor)

set(PRJ_APP_TITLE "LTR216 Monitor")


set(PRJ_VERSION_MAJOR    1)
set(PRJ_VERSION_MINOR    0)
set(PRJ_VERSION_REVISION 3)
set(PRJ_VERSION   ${PRJ_VERSION_MAJOR}.${PRJ_VERSION_MINOR}.${PRJ_VERSION_REVISION})

set(PRJ_VENDOR          "L Card")
set(PRJ_DESCRIPTION     "LTR216 Monitor")

string(TIMESTAMP CURRENT_YEAR "%Y")

if(WIN32)
    add_definitions(-DNOMINMAX)
    SET(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} /SUBSYSTEM:WINDOWS,5.01")
endif(WIN32)

configure_file(version.h.in ${CMAKE_CURRENT_BINARY_DIR}/version.h)

SET(LANGUAGES ru)
if(NOT PRJ_TRANSLATIONS_DIR)
    set(PRJ_TRANSLATIONS_DIR "translations")
endif(NOT PRJ_TRANSLATIONS_DIR)
add_definitions(-DPRJ_TRANSLATIONS_DIR="${PRJ_TRANSLATIONS_DIR}")


set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTORCC ON)

include_directories(${CMAKE_CURRENT_BINARY_DIR}
                    ${CMAKE_CURRENT_SOURCE_DIR}
                    lib/QLedIndicator
                    )

set(SOURCES
    main.cpp
    MainWindow.cpp
    MainWindow.h
    MainWindow.ui
    HierarchicalHeaderView.h
    HierarchicalHeaderView.cpp
    LTR216RecvThread.h
    LTR216RecvThread.cpp
    DataBlockProcessor.h
    DataBlockProcessor.cpp
    TareDialog.h
    TareDialog.cpp
    TareDialog.ui
    TareCoefsModel.h
    TareCoefsModel.cpp
    TareCoefsModelParams.h
    TareChannelsDialog.h
    TareChannelsDialog.cpp
    TareChannelsDialog.ui
    DeviceConfigDialog.h
    DeviceConfigDialog.cpp
    DeviceConfigDialog.ui
    ChannelsConfigWidget.h
    ChannelsConfigWidget.cpp
    ChannelsConfigWidget.ui
    SingleChConfigWidget.h
    SingleChConfigWidget.cpp
    SingleChConfigWidget.ui
    TablesViewDialog.h
    TablesViewDialog.cpp
    TablesViewDialog.ui
    DeviceConfig.h
    DeviceConfig.cpp
    FreqConfigWidget.h
    FreqConfigWidget.cpp
    ComboBoxNoWheel.h
    ComboBoxNoWheel.cpp
    CondLedIndicator.h
    CondLedIndicator.cpp
    MesValStateWidget.h
    MesValStateWidget.cpp
    MesValStateWidget.ui
    plot/Plot.h
    plot/Plot.cpp
    plot/PlotConfigDialog.h
    plot/PlotConfigDialog.cpp
    plot/PlotConfigDialog.ui
    plot/qcustomplot.h
    plot/qcustomplot.cpp
    lib/QLedIndicator/QLedIndicator.h
    lib/QLedIndicator/QLedIndicator.cpp

    icons/icons.qrc
    )


if(WIN32)
    configure_file(${PROJECT_NAME}.rc.in ${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}.rc)
    set(SOURCES ${SOURCES} ${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}.rc)
endif(WIN32)

find_package(Qt5Widgets)
find_package(Qt5PrintSupport)
find_package(Qt5LinguistTools)
set(LIBS ${LIBS} Qt5::Widgets Qt5::PrintSupport)

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_CURRENT_SOURCE_DIR}/cmake/Modules/")

find_package(LTRAPI REQUIRED ltrapi ltr216api)

include_directories(${LTRAPI_INCLUDE_DIRS})
add_definitions(${LTRAPI_DEFINITIONS})
set(LIBS ${LIBS} ${LTRAPI_LIBRARIES})

set(LMATH_DIR lib/lmath)
set(LMATH_USE_FFTW ON)
include(${LMATH_DIR}/lmath.cmake)
include_directories(${LMATH_DIR})
set(ADDITIONAL_FILES ${ADDITIONAL_FILES} ${LMATH_SOURCES} ${LMATH_HEADERS})
set(LIBS ${LIBS} ${LMATH_LIBS})

set(QTABLEVIEWHELPERS_DIR "${CMAKE_CURRENT_SOURCE_DIR}/lib/QTableViewHelpers")
include(${QTABLEVIEWHELPERS_DIR}/qtableviewhelpers.cmake)
include_directories(${QTABLEVIEWHELPERS_DIR})
set(ADDITIONAL_FILES ${ADDITIONAL_FILES} ${QTABLEVIEWHELPERS_FILES})

set(QTTRANSLATIONSWITCH_DIR lib/QtTranslationSwitch)
include(${QTTRANSLATIONSWITCH_DIR}/qttranslationswitch.cmake)
set(ADDITIONAL_FILES ${ADDITIONAL_FILES} ${QTTRANSLATIONSWITCH_FILES})



add_executable(${PROJECT_NAME} WIN32 ${SOURCES} ${ADDITIONAL_FILES} )

target_link_libraries(${PROJECT_NAME} ${LIBS})


if(WIN32)
    set(INSTALLER_NAME LTR216Monitor-setup-${PRJ_VERSION}.exe)

    include(winpaths.cmake)

    get_target_property(QtCoreLibrary Qt5::Core LOCATION)
    get_target_property(QtGuiLibrary Qt5::Gui LOCATION)
    get_target_property(QtWidgetsLibrary Qt5::Widgets LOCATION)

    get_target_property(QtWindowsLibrary Qt5::QWindowsIntegrationPlugin LOCATION)

    get_target_property (QT_QMAKE_EXECUTABLE Qt5::qmake IMPORTED_LOCATION)
    execute_process(COMMAND ${QT_QMAKE_EXECUTABLE} -query QT_INSTALL_TRANSLATIONS
                    OUTPUT_VARIABLE QT_TRANSLATIONS_DIR
                    )

    string(REGEX REPLACE "/" "\\\\" QtCoreLibrary    ${QtCoreLibrary})
    string(REGEX REPLACE "/" "\\\\" QtGuiLibrary     ${QtGuiLibrary})
    string(REGEX REPLACE "/" "\\\\" QtWidgetsLibrary ${QtWidgetsLibrary})
    string(REGEX REPLACE "/" "\\\\" QtWindowsLibrary ${QtWindowsLibrary})

    string(STRIP ${QT_TRANSLATIONS_DIR} QT_TRANSLATIONS_DIR)

    string(REGEX REPLACE "/" "\\\\" UCRT_REDISTR ${UCRT_REDISTR})
    string(REGEX REPLACE "/" "\\\\" MSVC_RT_REDISTR_DIR ${MSVC_RT_REDISTR_DIR})
    string(REGEX REPLACE "/" "\\\\" FFTW_DLL ${FFTW_DLL})
    string(REGEX REPLACE "/" "\\\\" LTRDLL_DIR ${LTRDLL_DIR})


    configure_file(win-installer/${PROJECT_NAME}.nsi.in
                   ${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}.nsi @ONLY)

    add_custom_command(OUTPUT ${INSTALLER_NAME}
        COMMAND ${NSIS_EXEC} ${PROJECT_NAME}.nsi
        DEPENDS ${PROJECT_NAME}
        DEPENDS ${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}.nsi
        WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
    )

    add_custom_target(${PROJECT_NAME}-installer DEPENDS ${INSTALLER_NAME})

    if(NOT WIN_INSTALLER_NOSIGN)
        #подписываем установщик при неободимости
        add_custom_command(TARGET ${PROJECT_NAME}-installer POST_BUILD
            COMMAND "${SIGNTOOL}" sign /v /n "${SIGN_COMPANY_NAME}" /t ${CERT_TIMESTAMP_URL} ${INSTALLER_NAME}
            WORKING_DIRECTORY ${PROJECT_BINARY_DIR}
        )
    endif(NOT WIN_INSTALLER_NOSIGN)
endif(WIN32)


source_group("Sources" ${PROJECT_SOURCE_DIR})
