#include "ChannelsConfigWidget.h"
#include "ui_ChannelsConfigWidget.h"
#include "DeviceConfig.h"
#include "ComboBoxNoWheel.h"
#include "TableHeightUpdater.h"

ChannelsConfigWidget::ChannelsConfigWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ChannelsConfigWidget),
    m_allChUpdate(false) {

    ui->setupUi(this);

}

ChannelsConfigWidget::~ChannelsConfigWidget() {
    delete ui;
}

void ChannelsConfigWidget::setConfig(DeviceConfig *cfg) {
    m_cfg = cfg;

    ui->adcChannels->setRowCount(LTR216_CHANNELS_CNT);

    for (unsigned ch=0; ch < LTR216_CHANNELS_CNT; ch++) {
        ChUi ch_ui;
        fillChUi(ui->adcChannels, ch, &ch_ui,  QString::number(ch+1));
        m_chUis.append(ch_ui);
    }

    ui->adcAllChannels->setRowCount(1);
    ui->adcAllChannels->setColumnCount(ui->adcChannels->columnCount());
    fillChUi(ui->adcAllChannels, 0, &m_allChUi, tr("Все каналы"));
    for (int i = 0; i < ui->adcChannels->columnCount(); i++) {
        ui->adcAllChannels->horizontalHeader()->resizeSection(i, ui->adcChannels->horizontalHeader()->sectionSize(i));
    }
    connect(ui->adcChannels->horizontalHeader(), SIGNAL(sectionResized(int,int,int)),
            SLOT(onChSectionResize(int,int,int)));


    connect(ui->adcChannels, SIGNAL(itemChanged(QTableWidgetItem*)),
            SLOT(onChannelItemChanged(QTableWidgetItem*)));
    connect(cfg, SIGNAL(ch16ModeChanged()), SLOT(updateCh16Mode()));

    connect(ui->adcAllChannels, SIGNAL(itemChanged(QTableWidgetItem*)), SLOT(allChEnChanged(QTableWidgetItem*)));
    connect(m_allChUi.range, SIGNAL(currentIndexChanged(int)), SLOT(allChRangeChanged()));

    new TableHeightUpdater(ui->adcChannels);
}

void ChannelsConfigWidget::showConfig() {
    m_allChUpdate = true;
    for (unsigned ch=0; ch < LTR216_CHANNELS_CNT; ch++) {
        m_chUis[ch].chItem->setCheckState(m_cfg->channelEnabled(ch) ? Qt::Checked : Qt::Unchecked);
        setComboBoxItemByData(m_chUis[ch].range, m_cfg->channelRange(ch));
    }
    m_allChUpdate = false;
    updateCh16Mode();
    updateAllChState();
}

void ChannelsConfigWidget::changeEvent(QEvent *e) {
    QWidget::changeEvent(e);
    switch (e->type()) {
        case QEvent::LanguageChange:
            ui->retranslateUi(this);
            break;
        default:
            break;
    }
}

void ChannelsConfigWidget::onRangeChanged() {
    QComboBox *sndBox = qobject_cast<QComboBox *>(sender());
    for (int ch_idx = 0; ch_idx < m_chUis.size(); ch_idx++) {
        if (m_chUis.at(ch_idx).range == sndBox) {
            m_cfg->setChannelRange(ch_idx, m_chUis.at(ch_idx).range->currentData().toInt());
            updateAllChState();
            break;
        }
    }
}

void ChannelsConfigWidget::onChannelItemChanged(QTableWidgetItem *item) {
    for (int ch_idx = 0; ch_idx < m_chUis.size(); ch_idx++) {
        if (m_chUis.at(ch_idx).chItem == item) {
            m_cfg->setChannelEnabled(ch_idx, item->checkState() == Qt::Checked);
            updateAllChState();
            break;
        }
    }
}

void ChannelsConfigWidget::updateCh16Mode() {
    ChUi chUi = m_chUis.last();
    Qt::ItemFlags cur_flags = chUi.chItem->flags();
    bool ch16_en = !m_cfg->ch16ForUref();
    if (ch16_en) {
        cur_flags |= Qt::ItemIsEnabled;
    } else {
        cur_flags &= ~Qt::ItemIsEnabled;
        chUi.chItem->setCheckState(Qt::Unchecked);
    }
    chUi.chItem->setFlags(cur_flags);
    chUi.range->setEnabled(ch16_en);
}

void ChannelsConfigWidget::allChEnChanged(QTableWidgetItem *item) {
    if (item == m_allChUi.chItem) {
        int checkState = item->checkState();
        if (checkState != Qt::PartiallyChecked) {
            if (!m_allChUpdate) {
                m_allChUpdate = true;
                bool en = checkState == Qt::Checked;
                for (int i = 0; i < m_chUis.size(); i++) {
                    if (m_chUis.at(i).chItem->flags() &  Qt::ItemIsEnabled) {
                        m_chUis.at(i).chItem->setCheckState(en ? Qt::Checked : Qt::Unchecked);
                    }
                }
                m_allChUpdate = false;
                updateAllChState();
            }
        }
    }
}

void ChannelsConfigWidget::allChRangeChanged() {
    if (!m_allChUpdate) {
        m_allChUpdate = true;
        int range = m_allChUi.range->currentData().toInt();
        if (range >= 0) {
            for (int i = 0; i < m_chUis.size(); i++) {
                setComboBoxItemByData(m_chUis.at(i).range, range);
            }
        }
        m_allChUpdate = false;
        updateAllChState();
    }
}

void ChannelsConfigWidget::onChSectionResize(int logicalIndex, int oldSize, int newSize) {
    if (logicalIndex == 0) {
        ui->adcAllChannels->horizontalHeader()->resizeSection(logicalIndex, newSize);
    }
}

void ChannelsConfigWidget::updateAllChState() {
    if (!m_allChUpdate) {
        m_allChUpdate = true;
        Qt::CheckState en_state = m_chUis.first().chItem->checkState();
        int range = m_chUis.first().range->currentData().toInt();

        for (int i = 1; i < m_chUis.size(); i++) {
            if (m_chUis.at(i).chItem->flags() &  Qt::ItemIsEnabled) {
                if (en_state != m_chUis.at(i).chItem->checkState())
                    en_state = Qt::PartiallyChecked;
                if (range != m_chUis.at(i).range->currentData().toInt())
                    range = -1;
            }
        }

        m_allChUi.chItem->setCheckState(en_state);
        if (range < 0) {
            m_allChUi.range->setCurrentIndex(-1);
        } else {
            setComboBoxItemByData(m_allChUi.range, range);
        }
        m_allChUpdate = false;
    }
}

void ChannelsConfigWidget::updateConfig() {
    for (int ch_idx = 0; ch_idx < m_chUis.size(); ch_idx++) {
        m_cfg->setChannelEnabled(ch_idx, m_chUis.at(ch_idx).chItem->checkState() == Qt::Checked);
        m_cfg->setChannelRange(ch_idx, m_chUis.at(ch_idx).range->currentData().toInt());
    }
}

void ChannelsConfigWidget::fillChUi(QTableWidget *table, int row, ChannelsConfigWidget::ChUi *chUi, QString name) {
    chUi->chItem = new QTableWidgetItem();
    chUi->chItem->setText(name);

    chUi->range = new ComboBoxNoWheel();
    addRangeItem(chUi->range, 35, LTR216_RANGE_35);
    addRangeItem(chUi->range, 70, LTR216_RANGE_70);

    connect(chUi->range, SIGNAL(currentIndexChanged(int)), SLOT(onRangeChanged()));


    table->setItem(row, 0, chUi->chItem);
    table->setCellWidget(row, 1, chUi->range);
}

void ChannelsConfigWidget::addRangeItem(QComboBox *box, double val, unsigned code) {
    box->addItem("± " + QString::number(val) + " мВ",  code);
}

void ChannelsConfigWidget::setComboBoxItemByData(QComboBox *box, int data) {
    int idx = 0;
    for (int i=0; i < box->count(); i++) {
        if (box->itemData(i).toInt()==data) {
            idx = i;
            break;
        }
    }
    box->setCurrentIndex(idx);
}
