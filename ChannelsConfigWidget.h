#ifndef CHANNELSCONFIGWIDGET_H
#define CHANNELSCONFIGWIDGET_H

#include <QWidget>
class QComboBox;
class QTableWidgetItem;
class QTableWidget;

namespace Ui {
class ChannelsConfigWidget;
}

class DeviceConfig;


class ChannelsConfigWidget : public QWidget {
    Q_OBJECT

public:
    explicit ChannelsConfigWidget(QWidget *parent = 0);
    ~ChannelsConfigWidget();

    void setConfig(DeviceConfig *cfg);
public Q_SLOTS:
    void showConfig();
    void updateConfig();
protected:
    void changeEvent(QEvent *e);
private Q_SLOTS:
    void onRangeChanged();
    void onChannelItemChanged(QTableWidgetItem* item);
    void updateCh16Mode();    

    void allChEnChanged(QTableWidgetItem *item);
    void allChRangeChanged();
    void onChSectionResize(int logicalIndex, int oldSize, int newSize);
private:    
    struct ChUi {
        QTableWidgetItem *chItem;
        QComboBox *range;
    };

    void updateAllChState();
    void fillChUi(QTableWidget *table, int row, ChUi *chUi, QString name);
    void addRangeItem(QComboBox *box, double val, unsigned code);
    void setComboBoxItemByData(QComboBox *box, int data);

    Ui::ChannelsConfigWidget *ui;
    DeviceConfig *m_cfg;

    QList<ChUi> m_chUis;
    ChUi m_allChUi;
    bool m_allChUpdate;
};

#endif // CHANNELSCONFIGWIDGET_H
