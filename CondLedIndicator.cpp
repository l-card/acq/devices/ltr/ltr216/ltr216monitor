#include "CondLedIndicator.h"
#include "QHBoxLayout"
#include <QMouseEvent>
#include <QScopedPointer>

CondLedIndicator::CondLedIndicator(QWidget *parent) : QWidget(parent) {
    QHBoxLayout *lout = new QHBoxLayout();
    lout->setAlignment(Qt::AlignCenter);
    lout->setContentsMargins(0,0,0,0);
    lout->addWidget(m_led = new QLedIndicator(0, 22));

    m_led->setEnabled(false);
    m_led->installEventFilter((QObject*)this);
    setLayout(lout);
    setState(StateOff);
}

void CondLedIndicator::setState(CondLedIndicator::State state) {
    m_state = state;
    switch (state) {
        case CondLedIndicator::StateOk:
            m_led->setOnColor1(QColor(0, 255, 0));
            m_led->setOnColor2(QColor(0, 192,0));
            m_led->setChecked(true);
            m_led->update();
            break;
        case CondLedIndicator::StateBad:
            m_led->setOnColor1(QColor(255,0,0));
            m_led->setOnColor2(QColor(192,0,0));
            m_led->setChecked(true);
            m_led->update();
            break;
        case CondLedIndicator::StateDisabled:
            m_led->setOnColor1(Qt::darkGray);
            m_led->setOnColor2(Qt::gray);
            m_led->setChecked(true);
            m_led->update();
            break;
        case CondLedIndicator::StateZ:
            m_led->setOnColor1(QColor(0,0,255));
            m_led->setOnColor2(QColor(0,0,192));
            m_led->setChecked(true);
            m_led->update();
            break;
        default:
            m_led->setChecked(false);
            break;
    }
}


void CondLedIndicator::setVal(bool val) {
    if (val) {
        setStateRed();
    } else {
        setStateGreen();
    }
}

void CondLedIndicator::setStateRed() {
    setState(StateBad);
}

void CondLedIndicator::setStateGreen() {
    setState(StateOk);
}

void CondLedIndicator::setStateOff() {
    setState(StateOff);
}

void CondLedIndicator::mousePressEvent(QMouseEvent *event) {
    if (event->button() == Qt::RightButton) {
        emit customContextMenuRequested(event->pos());
    }
}

bool CondLedIndicator::eventFilter(QObject *object, QEvent *event) {
    bool ret = false;
    QLedIndicator *ind = qobject_cast<QLedIndicator *>(object);
    if (ind) {
        if (event->type() == QEvent::MouseButtonPress) {
            QMouseEvent *mevt = dynamic_cast<QMouseEvent *>(event);
            if (mevt) {                
                QScopedPointer<QMouseEvent> procEvt (new QMouseEvent(mevt->type(), mapFromGlobal(m_led->mapToGlobal(mevt->pos())),
                                                                     mevt->button(), mevt->buttons(), mevt->modifiers()));
                mousePressEvent(procEvt.data());
            }
        }
    }
    return ret;
}
