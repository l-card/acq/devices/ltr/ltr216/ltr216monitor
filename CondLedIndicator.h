#ifndef MEASCONDLEDINDICATION_H
#define MEASCONDLEDINDICATION_H

#include "QLedIndicator.h"

class CondLedIndicator : public QWidget {
    Q_OBJECT
public:
    CondLedIndicator(QWidget *parent = 0);

    enum State {
        StateOk,
        StateBad,
        StateOff,
        StateDisabled,
        StateZ
    };

    void setState(State state);
    void setVal(bool val);
    void setStateRed();
    void setStateGreen();
    void setStateOff();

    State state() const {return m_state;}

    QLedIndicator *led() const {return m_led;}
protected:
    void mousePressEvent(QMouseEvent *event);

    bool eventFilter(QObject *object, QEvent *event);
private:
    QLedIndicator *m_led;
    State m_state;
};

#endif // MEASCONDLEDINDICATION_H
