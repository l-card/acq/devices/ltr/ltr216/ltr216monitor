#include "DataBlockProcessor.h"
#include "math.h"

DataBlockProcessor::DataBlockProcessor() : m_wintype(LMATH_WINTYPE_BH_4TERM) {
    qRegisterMetaType<QSharedPointer<DataBlockProcessor::DataBlock> >("QSharedPointer<DataBlockProcessor::DataBlock>");
}

void DataBlockProcessor::setParams(unsigned block_size, double dt, int wintype) {
    m_ctx.setParams(block_size, dt, wintype);
}

void DataBlockProcessor::processFrame(QSharedPointer<LTR216RecvThread::Frame> frame) {
    unsigned block_size = frame->chData.at(0)->data.size();
    double   dt = frame->chData.at(0)->dt;

    if ((m_ctx.x_vals.size() == 0) || (block_size != m_ctx.x_vals.size()) || (dt != m_ctx.x_vals.at(0))) {
        setParams(block_size, dt, m_wintype);
    }


    QSharedPointer<DataBlock> procBlock(new DataBlock());
    procBlock->status = frame->status;

    for (int ch_num = 0; ch_num < frame->chData.size(); ch_num++) {
        QSharedPointer<LTR216RecvThread::Data> chData = frame->chData.at(ch_num);
        QSharedPointer<ChDataBlock> chProcData(new ChDataBlock());
        chProcData->status = chData->status;

        chProcData->x_vals = m_ctx.x_vals;
        chProcData->amp_vals = chData->data;

        chProcData->fft_x_vals = m_ctx.fft_x_vals;



        lmath_acdc_estimation_with_window(chProcData->amp_vals.data(), m_ctx.win_ctx,
                                          m_ctx.tmp_buf.data(), &chProcData->dc, &chProcData->ac);

        chProcData->range_value = chData->max_value;

        if (chProcData->fft_x_vals.size() > 3) {
            lmath_window_apply_scaled(chProcData->amp_vals.data(), m_ctx.win_ctx, m_ctx.win_sig);

            chProcData->fft_db.resize(m_ctx.fft_size);
            lmath_fft_r2c_calc_amp_pha_spectrum_peak(m_ctx.fft_ctx, chProcData->fft_db.data(), NULL);


            double df = chProcData->fft_x_vals.at(1);
            chProcData->peak.valid = (lmath_find_peak_freq_spectrum(
                                     chProcData->fft_db.data(), chProcData->fft_db.size(),
                                     df, -1, m_ctx.win_ctx,
                                     &chProcData->peak.freq, &chProcData->peak.amp) == 0);
            if (chProcData->peak.valid) {
                chProcData->peak.amp = sqrt(chProcData->peak.amp);
            }

            if (chProcData->peak.valid) {
                chProcData->spectrum_params.valid =
                        (lmath_calc_spectrum_params(chProcData->fft_db.data(), chProcData->fft_db.size(), df,
                                                    chProcData->peak.freq, 6, m_ctx.win_ctx,
                                                    &chProcData->spectrum_params.snr,
                                                    &chProcData->spectrum_params.thd,
                                                    &chProcData->spectrum_params.sinad,
                                                    &chProcData->spectrum_params.sfdr,
                                                    &chProcData->spectrum_params.enob
                                                   ) == 0);
            }



            /* перевод в децибелы */
            for (int i=0; i < chProcData->fft_db.size(); i++) {
                chProcData->fft_db[i] = 20.0*log10(chProcData->fft_db[i]/chProcData->range_value);
            }
        }

        procBlock->chData.append(chProcData);
    }

    Q_EMIT newBlock(procBlock);
}

void DataBlockProcessor::processParamsUpdate(int wintype) {
    if ((m_ctx.sig_size != 0) && (m_wintype != wintype)) {
        m_wintype = wintype;
        setParams(m_ctx.sig_size, m_ctx.dt, m_wintype);
    }
}

void DataBlockProcessor::ProcContext::clear() {
    if (fft_ctx) {
        lmath_fft_r2c_context_destroy(fft_ctx);
        fft_ctx = NULL;
    }
    if (win_ctx) {
        lmath_window_context_destroy(win_ctx);
        win_ctx = NULL;
    }
}

void DataBlockProcessor::ProcContext::setParams(int points_cnt, double init_dt, int wintype) {
    clear();
    sig_size = points_cnt;
    dt = init_dt;
    x_vals.resize(points_cnt);
    for (int i = 0; i < points_cnt; i++)
        x_vals[i] = 1000.*i*dt;

    tmp_buf.resize(points_cnt);

    win_ctx = lmath_window_context_create(wintype, NULL, points_cnt);
    fft_ctx = lmath_fft_r2c_context_create(points_cnt, 0);
    /* в качестве входного сигнала для FFT используем исходный сигнал
       после применения окна. используем один буфер, чтобы не копировать данные */
    win_sig = lmath_fft_r2c_context_in_buf(fft_ctx);
    fft_size = lmath_fft_r2c_context_fft_size(fft_ctx);
    fft_x_vals.resize(fft_size);

    double df = lmath_fft_r2c_context_df(fft_ctx, dt);
    for (int i = 0; i < fft_size; i++)
        fft_x_vals[i] = i*df;
}
