#ifndef DATABLOCKPROCESSOR_H
#define DATABLOCKPROCESSOR_H

#include <QObject>
#include "LTR216RecvThread.h"
#include "lmath.h"

class DataBlockProcessor : public QObject {
    Q_OBJECT
public:


    struct ChDataBlock {
        TLTR216_DATA_CH_STATUS status;

        QVector<double> x_vals;
        QVector<double> amp_vals;
        QVector<double> fft_x_vals;
        QVector<double> fft_db;
        double dc;
        double ac;
        double range_value;

        struct {
            bool valid;
            double freq;
            double amp;
        } peak;

        struct {
            bool valid;
            double snr;
            double thd;
            double sinad;
            double sfdr;
            double enob;
        } spectrum_params;
    };

    struct DataBlock {
        TLTR216_DATA_STATUS status;
        QList<QSharedPointer<ChDataBlock> > chData;
    };


    DataBlockProcessor();

Q_SIGNALS:
    void newBlock(QSharedPointer<DataBlockProcessor::DataBlock> block);
public Q_SLOTS:

    void processFrame(QSharedPointer<LTR216RecvThread::Frame> frame);
    void processParamsUpdate(int wintype);
private:
    void setParams(unsigned block_size, double dt, int wintype);



    struct ProcContext {
        ProcContext() : win_ctx(0), fft_ctx(0), win_sig(0), sig_size(0), dt(0) {
        }
        ~ProcContext() {
            clear();
        }

        void clear();
        void setParams(int points_cnt, double init_dt, int wintype);

        int ctx_num;
        QVector<double> x_vals;
        QVector<double> fft_x_vals;

        t_lmath_window_context win_ctx;
        t_lmath_fft_r2c_context fft_ctx;
        double *win_sig;
        QVector<double> tmp_buf;
        int fft_size;
        int sig_size;
        double dt;
    };

    ProcContext m_ctx;
    int m_wintype;



};

#endif // DATABLOCKPROCESSOR_H
