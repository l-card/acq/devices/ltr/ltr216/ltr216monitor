#include "DeviceConfig.h"
#include <QSettings>

DeviceConfig::DeviceConfig() {
    memset(&m_params, 0, sizeof(m_params));
}

DeviceConfig::DeviceConfig(const DeviceConfig &cfg) {
    memcpy(&m_params, &cfg.m_params, sizeof(m_params));
}

void DeviceConfig::load(QSettings &set) {
    set.beginGroup("LTR216MonitorDeviceConfig");
    setISrcValue(25);
    m_params.iSrcCode = set.value("iSrcCode", m_params.iSrcCode).toInt();
    m_params.adcFreq = set.value("AdcFreq", 20000).toDouble();
    m_params.swTimeUs = set.value("SwTimeUs", 10).toDouble();
    setMeasTime(set.value("MeasTime", 1000).toInt());
    m_params.tareCoefEn = set.value("TareCoefEn").toBool();
    m_params.ch16forUref = set.value("Ch16ForUref").toBool();
    m_params.singleChMode = set.value("SingleChMode").toBool();

    m_params.bgMeasMask = set.value("BgMeasMask").toInt();
    m_params.bgMeasGroup = 0;
    QList<const BgMeasGroup *> groups = defaultBgMeasGroups();
    QString groupName = set.value("BgMeasGroup").toString();
    if (groupName.isEmpty()) {
        setBgMeasGroup(groups.at(0));
    } else {
        Q_FOREACH(const BgMeasGroup *group, groups) {
            if (group->name == groupName) {
                setBgMeasGroup(group);
                break;
            }
        }
    }

    m_params.shortRThresh = set.value("ShortRThreash", LTR216_DEFAULT_SHORT_THRESH_R).toDouble();
    m_params.cableLength = set.value("CableLength", LTR216_DEFAULT_CABLE_LENGTH).toDouble();
    m_params.cableCapPerUnit = set.value("CableCapactiyPerUnit", LTR216_DEFAULT_CABLE_CAPACITY_PER_UNIT).toDouble();



    m_params.filterManualEn = set.value("FilterManualEn").toBool();
    m_params.filterManualType = set.value("FilterManualType", LTR216_FILTER_SINC5_1).toInt();
    m_params.filterManualOdr = set.value("FilterManualOdr", 1).toInt();

    int cnt = set.beginReadArray("AdcChannels");
    for (int ch_idx = 0; ch_idx < LTR216_CHANNELS_CNT; ch_idx++) {
        if (ch_idx < cnt) {
            set.setArrayIndex(ch_idx);
            m_params.channels[ch_idx].enabled = set.value("Enabled").toBool();
            m_params.channels[ch_idx].range = set.value("Range").toInt();
        } else {
            m_params.channels[ch_idx].enabled = false;
            m_params.channels[ch_idx].range = LTR216_RANGE_35;
        }
    }
    set.endArray();
    set.endGroup();

    updateFilterParams();
}

void DeviceConfig::save(QSettings &set) const {
    set.beginGroup("LTR216MonitorDeviceConfig");
    set.setValue("iSrcCode", m_params.iSrcCode);
    set.setValue("AdcFreq", m_params.adcFreq);
    set.setValue("SwTimeUs", m_params.swTimeUs);
    set.setValue("MeasTime", m_params.measTime);
    set.setValue("TareCoefEn", m_params.tareCoefEn);
    set.setValue("Ch16ForUref", m_params.ch16forUref);
    set.setValue("SingleChMode", m_params.singleChMode);

    set.setValue("BgMeasMask", m_params.bgMeasMask);
    set.setValue("BgMeasGroup", m_params.bgMeasGroup ? m_params.bgMeasGroup->name : "Custom");
    set.setValue("ShortRThreash", m_params.shortRThresh);
    set.setValue("CableLength", m_params.cableLength);
    set.setValue("CableCapactiyPerUnit", m_params.cableCapPerUnit);

    set.setValue("FilterManualEn", m_params.filterManualEn);
    set.setValue("FilterManualType", m_params.filterManualType);
    set.setValue("FilterManualOdr", m_params.filterManualOdr);

    set.beginWriteArray("AdcChannels", LTR216_CHANNELS_CNT);
    for (int ch_idx = 0; ch_idx < LTR216_CHANNELS_CNT; ch_idx++) {
        set.setArrayIndex(ch_idx);
        set.setValue("Enabled", m_params.channels[ch_idx].enabled);
        set.setValue("Range", m_params.channels[ch_idx].range);
    }
    set.endArray();

    set.endGroup();
}

bool DeviceConfig::hasConfig(QSettings &set) {
    return set.childGroups().contains("LTR216MonitorDeviceConfig");
}

double DeviceConfig::iSrcValue() const {
    double isrc;
    LTR216_CalcISrcValue(NULL, m_params.iSrcCode, &isrc);
    return isrc;
}

e_LTR216_ADC_SWMODE DeviceConfig::adcSwMode() const {
    return m_params.singleChMode ? LTR216_ADC_SWMODE_SIGNLECH_CONT :
                                   LTR216_ADC_SWMODE_MULTICH_SYNC;
}

int DeviceConfig::enabledChannelsCnt() const {
    int ch_cnt = 0;
    for (int ch_idx = 0; ch_idx < LTR216_CHANNELS_CNT; ch_idx++) {
        if (m_params.channels[ch_idx].enabled)
            ch_cnt++;
    }
    return ch_cnt;
}

QList<const DeviceConfig::BgMeasGroup *> DeviceConfig::defaultBgMeasGroups() {
    static const BgMeasGroup f_all(tr("Все измерения"), LTR216_BG_MEASGROUP_ALL);
    static const BgMeasGroup f_noopen(tr("Без проверки обрыва"), LTR216_BG_MEASGROUP_ALL & ~LTR216_BG_MEASGROUP_OPEN);
    static const BgMeasGroup f_nosectbl(tr("Разбаланс и ноль"), LTR216_BG_MEASGROUP_ALL & ~LTR216_BG_MEASGROUP_SECTBL);
    static const BgMeasGroup f_none(tr("Только разбаланс"), 0);
    return QList<const DeviceConfig::BgMeasGroup *>() << &f_all << &f_noopen << &f_nosectbl << &f_none;
}

bool DeviceConfig::bgMeasOffs() const {
    return bgMeas(LTR216_BG_MEAS_OFFS);
}

bool DeviceConfig::bgMeasUref() const {
    return bgMeas(LTR216_BG_MEAS_UREF);
}

bool DeviceConfig::bgMeasUrefOffs() const {
    return bgMeas(LTR216_BG_MEAS_UREF_OFFS);
}

bool DeviceConfig::bgMeasVadj() const {
    return bgMeas(LTR216_BG_MEAS_VADJ);
}

bool DeviceConfig::bgMeasUneg() const {
    return bgMeas(LTR216_BG_MEAS_UNEG);
}

bool DeviceConfig::bgMeasUx() const {
    return bgMeas(LTR216_BG_MEAS_CH_UX);
}

bool DeviceConfig::bgMeasUcm() const {
    return bgMeas(LTR216_BG_MEAS_CH_CM);
}

bool DeviceConfig::bgMeasCheckOpenUneg() const {
    return bgMeas(LTR216_BG_MEAS_UNEG_OPEN);
}

bool DeviceConfig::bgMeasCheckOpenUref() const {
    return bgMeas(LTR216_BG_MEAS_UREF_OPEN);
}

bool DeviceConfig::bgMeasCheckOpenUx() const {
    return bgMeas(LTR216_BG_MEAS_CH_OPEN);
}

bool DeviceConfig::bgMeasCheckShortUref() const {
    return bgMeas(LTR216_BG_MEAS_UREF_SHORT);
}

bool DeviceConfig::bgMeasCheckShortUx() const {
    return bgMeas(LTR216_BG_MEAS_CH_SHORT);
}

void DeviceConfig::setBgMeasOffs(bool en) {
    setBgMeas(LTR216_BG_MEAS_OFFS, en);
}

void DeviceConfig::setBgMeasUref(bool en) {
    setBgMeas(LTR216_BG_MEAS_UREF, en);
}

void DeviceConfig::setBgMeasUrefOffs(bool en) {
    setBgMeas(LTR216_BG_MEAS_UREF_OFFS, en);
}

void DeviceConfig::setBgMeasVadj(bool en) {
    setBgMeas(LTR216_BG_MEAS_VADJ, en);
}

void DeviceConfig::setBgMeasUneg(bool en) {
    setBgMeas(LTR216_BG_MEAS_UNEG, en);
}

void DeviceConfig::setBgMeasUx(bool en) {
    setBgMeas(LTR216_BG_MEAS_CH_UX, en);
}

void DeviceConfig::setBgMeasUcm(bool en) {
    setBgMeas(LTR216_BG_MEAS_CH_CM, en);
}

void DeviceConfig::setBgMeasCheckOpenUneg(bool en) {
    setBgMeas(LTR216_BG_MEAS_UNEG_OPEN, en);
}

void DeviceConfig::setBgMeasCheckOpenUref(bool en) {
    setBgMeas(LTR216_BG_MEAS_UREF_OPEN, en);
}

void DeviceConfig::setBgMeasCheckOpenUx(bool en) {
    setBgMeas(LTR216_BG_MEAS_CH_OPEN, en);
}

void DeviceConfig::setBgMeasCheckShortUref(bool en) {
    setBgMeas(LTR216_BG_MEAS_UREF_SHORT, en);
}

void DeviceConfig::setBgMeasCheckShortUx(bool en) {
    setBgMeas(LTR216_BG_MEAS_CH_SHORT, en);
}

void DeviceConfig::setAdcFreq(double freq) {
    if (!singleChMode()) {
        LTR216_FindSyncFreqDiv(freq, NULL, &m_params.adcFreq);
        Q_EMIT adcFreqUpdated(adcFreq());
    } else {
        m_params.adcFreq = freq;
    }
    updateFilterParams();
    Q_EMIT frameParamsChanged();
}

void DeviceConfig::setSwTimeUs(double us) {
    m_params.swTimeUs = us;
    updateFilterParams();
}

void DeviceConfig::setMeasTime(int ms) {
    if (ms < 100)
        ms = 100;
    m_params.measTime = ms;
}

void DeviceConfig::setSingleChMode(bool singleCh) {
    if (singleChMode() != singleCh) {
        m_params.singleChMode = singleCh;
        setAdcFreq(adcFreq());
        Q_EMIT singleChModeChanged(singleCh);
    }
}

void DeviceConfig::setTareCoefEn(bool en) {
    if (m_params.tareCoefEn != en) {
        m_params.tareCoefEn = en;
        Q_EMIT frameParamsChanged();
    }
}

void DeviceConfig::setCh16ForUref(bool en) {
    if (m_params.ch16forUref != en) {
        m_params.ch16forUref = en;
        if (!en)
            m_params.channels[uref_ch_idx].enabled = false;
        Q_EMIT frameParamsChanged();
        Q_EMIT ch16ModeChanged();
    }
}

void DeviceConfig::fillDeviceConfig(TLTR216 *hmodule) {
    hmodule->Cfg.AdcSwMode = adcSwMode();
    hmodule->Cfg.AdcMinSwTimeUs = m_params.swTimeUs;

    hmodule->Cfg.ISrcCode = m_params.iSrcCode;
    if (!singleChMode()) {
        LTR216_FindSyncFreqDiv(m_params.adcFreq, &hmodule->Cfg.SyncFreqDiv, NULL);
    }
    hmodule->Cfg.AdcOdrCode = m_params.filterParams.AdcOdrCode;
    hmodule->Cfg.FilterType = m_params.filterParams.FilterType;

    hmodule->Cfg.TareEnabled = m_params.tareCoefEn;
    hmodule->Cfg.Ch16ForUref = m_params.ch16forUref;

    hmodule->Cfg.BgMeas = m_params.bgMeasMask;

    hmodule->Cfg.ShortThresholdR = m_params.shortRThresh;
    hmodule->Cfg.CableLength = m_params.cableLength;
    hmodule->Cfg.CableCapacityPerUnit = m_params.cableCapPerUnit;

    int cur_lch_pos = 0;
    for (int ch_idx = 0; ch_idx < LTR216_CHANNELS_CNT; ch_idx++) {
        if (m_params.channels[ch_idx].enabled) {
            hmodule->Cfg.LChTbl[cur_lch_pos].PhyChannel = ch_idx;
            hmodule->Cfg.LChTbl[cur_lch_pos].Range = m_params.channels[ch_idx].range;
            cur_lch_pos++;
        }
    }
    hmodule->Cfg.LChCnt = cur_lch_pos;
}

void DeviceConfig::setISrcValue(double value) {
    DWORD code;
    LTR216_FindISrcCode(NULL, value, &code, &value);
    m_params.iSrcCode = code;
}

void DeviceConfig::setFilterManualEnabled(bool en) {
    if (en != m_params.filterManualEn) {
        m_params.filterManualEn = en;
        updateFilterParams();
    }
}

void DeviceConfig::setFilterManualParams(int type, int odr) {
    m_params.filterManualType = type;
    m_params.filterManualOdr = odr;
    if (m_params.filterManualEn) {
        updateFilterParams();
    }
}



void DeviceConfig::setChannelEnabled(int ch, bool en) {
    if (ch16ForUref() && (ch == uref_ch_idx))
        en = false;

    if (m_params.channels[ch].enabled != en) {
        m_params.channels[ch].enabled = en;
        Q_EMIT frameParamsChanged();
    }
}

void DeviceConfig::setShortCheckRTresh(double val) {
    m_params.shortRThresh = val;
}

void DeviceConfig::setCableLengt(double len) {
    if (m_params.cableLength != len) {
        m_params.cableLength = len;
        Q_EMIT frameParamsChanged();
    }
}

void DeviceConfig::setCableCapPerUnit(double c) {
    if (m_params.cableCapPerUnit != c) {
        m_params.cableCapPerUnit = c;
        Q_EMIT frameParamsChanged();
    }
}

void DeviceConfig::setBgMeasGroup(const DeviceConfig::BgMeasGroup *group) {
    if (m_params.bgMeasGroup != group) {
        m_params.bgMeasGroup = group;
        if (group)
            m_params.bgMeasMask = group->bgMeasMask;
        Q_EMIT frameParamsChanged();
    }
}


void DeviceConfig::setChannelRange(int ch, int range) {
    if (range != m_params.channels[ch].range) {
        m_params.channels[ch].range = range;
        Q_EMIT frameParamsChanged();
    }
}

void DeviceConfig::updateFilterParams() {
    if (m_params.filterManualEn) {
        LTR216_GetFilterOutParams(adcSwMode(), m_params.filterManualType,
                                  m_params.filterManualOdr, &m_params.filterParams);
        if (singleChMode()) {
            m_params.resultSwTimeUs = 0;
        } else {
            m_params.resultSwTimeUs = (1./adcFreq() - 1./m_params.filterParams.Odr) * 1000000;
        }
    } else {
        LTR216_FindFilterParams(adcSwMode(), m_params.adcFreq, m_params.swTimeUs,
                               &m_params.filterParams, &m_params.resultSwTimeUs);
    }
    Q_EMIT filterParamsUpdated();
    if (singleChMode()) {
        m_params.adcFreq = m_params.filterParams.Odr;
        Q_EMIT adcFreqUpdated(adcFreq());
    }
}

bool DeviceConfig::bgMeas(int msk) const {
    return (m_params.bgMeasMask & msk) && !singleChMode();
}

void DeviceConfig::setBgMeas(int msk, bool en) {
    bool cur_en = m_params.bgMeasMask & msk;
    if ((en != cur_en) && !m_params.bgMeasGroup) {
        if (en) {
            m_params.bgMeasMask |= msk;
        } else {
            m_params.bgMeasMask &= ~msk;
        }
        Q_EMIT frameParamsChanged();
    }
}


