#ifndef DEVICECONFIG_H
#define DEVICECONFIG_H

#include <QObject>
#include "ltr/include/ltr216api.h"
class QSettings;



class DeviceConfig : public QObject {
    Q_OBJECT
public:
    DeviceConfig();
    DeviceConfig(const DeviceConfig &cfg);

    class BgMeasGroup {
    public:
        BgMeasGroup(QString iname, unsigned imsk) :
            name(iname), bgMeasMask(imsk) {}

        QString name;
        unsigned bgMeasMask;
    };

    void load(QSettings &set);
    void save(QSettings &set) const;
    static bool hasConfig(QSettings &set);

    double iSrcValue() const;
    double adcFreq() const {return m_params.adcFreq;}
    double swTimeUs() const {return m_params.swTimeUs;}
    double resultSwTimeUs() const {return m_params.resultSwTimeUs;}
    e_LTR216_ADC_SWMODE adcSwMode() const;
    bool singleChMode() const {return m_params.singleChMode;}

    int maesTime() const {return m_params.measTime;}
    bool filterManualEnabled() const {return m_params.filterManualEn;}
    const TLTR216_FILTER_OUT_PARAMS &filterParams() const {return m_params.filterParams;}

    bool channelEnabled(int ch) const {return m_params.channels[ch].enabled;}
    int channelRange(int ch) const {return m_params.channels[ch].range;}

    int enabledChannelsCnt() const;

    bool tareCoefEnabled() const {return m_params.tareCoefEn;}
    bool ch16ForUref() const {return m_params.ch16forUref;}


    const BgMeasGroup *bgMeasGroup() const {return m_params.bgMeasGroup;}
    static QList<const BgMeasGroup *>  defaultBgMeasGroups();

    bool bgMeasOffs() const;
    bool bgMeasUref() const;
    bool bgMeasUrefOffs() const;
    bool bgMeasVadj() const;
    bool bgMeasUneg() const;
    bool bgMeasUx() const;
    bool bgMeasUcm() const;
    bool bgMeasCheckOpenUneg() const;
    bool bgMeasCheckOpenUref() const;
    bool bgMeasCheckOpenUx() const;
    bool bgMeasCheckShortUref() const;
    bool bgMeasCheckShortUx() const;

    bool bgMeasIsUxUsed() const {return bgMeasUx() || bgMeasUcm();}

    double shortCheckRThresh() const {return m_params.shortRThresh;}
    double cableLength() const {return m_params.cableLength;}
    double cableCapacityPerUnit() const {return m_params.cableCapPerUnit;}

    static const int uref_ch_idx = 15;
Q_SIGNALS:
    void filterParamsUpdated();
    void adcFreqUpdated(double val);
    void frameParamsChanged();
    void ch16ModeChanged();
    void singleChModeChanged(bool singleCh);
public Q_SLOTS:
    void fillDeviceConfig(TLTR216 *hmodule);

    void setISrcValue(double value);
    void setAdcFreq(double freq);
    void setSwTimeUs(double us);
    void setMeasTime(int ms);

    void setSingleChMode(bool singleCh);

    void setTareCoefEn(bool en);
    void setCh16ForUref(bool en);

    void setFilterManualEnabled(bool en);
    void setFilterManualParams(int type, int odr);

    void setChannelRange(int ch, int range);
    void setChannelEnabled(int ch, bool en);

    void setShortCheckRTresh(double val);
    void setCableLengt(double len);
    void setCableCapPerUnit(double c);

    void setBgMeasGroup(const BgMeasGroup *group);
    void setBgMeasOffs(bool en);
    void setBgMeasUref(bool en);
    void setBgMeasUrefOffs(bool en);
    void setBgMeasVadj(bool en);
    void setBgMeasUneg(bool en);
    void setBgMeasUx(bool en);
    void setBgMeasUcm(bool en);
    void setBgMeasCheckOpenUneg(bool en);
    void setBgMeasCheckOpenUref(bool en);
    void setBgMeasCheckOpenUx(bool en);
    void setBgMeasCheckShortUref(bool en);
    void setBgMeasCheckShortUx(bool en);
private:
    void updateFilterParams();

private:
    bool bgMeas(int msk) const;
    void setBgMeas(int msk, bool en);

    struct {
        int iSrcCode;
        bool singleChMode;
        double adcFreq;
        double swTimeUs;
        double resultSwTimeUs;
        double measTime;

        bool tareCoefEn;
        bool ch16forUref;

        const BgMeasGroup *bgMeasGroup;
        unsigned bgMeasMask;
        double shortRThresh;
        double cableLength;
        double cableCapPerUnit;

        bool filterManualEn;
        int filterManualType;
        int filterManualOdr;
        TLTR216_FILTER_OUT_PARAMS filterParams;

        struct {
            bool enabled;
            int  range;
        } channels[LTR216_CHANNELS_CNT];
    } m_params;
};

#endif // DEVICECONFIG_H
