#include "DeviceConfigDialog.h"
#include <QLocale>
#include <QSettings>
#include <QMessageBox>
#include "ComboBoxNoWheel.h"
#include "TareDialog.h"
#include "TablesViewDialog.h"
#include "ui_DeviceConfigDialog.h"
#include <QToolBar>
#include <QFileDialog>
#include <QKeyEvent>
#include <QToolButton>
#include "lqtdefs.h"

Q_DECLARE_METATYPE(const DeviceConfig::BgMeasGroup *);

static const struct {
    int code;
    double single_odr;
    double multi_odr;
    double notch;
} filt_sinc5_pars[] = {
    { 0, 250000,    50000,    250000},
    { 1, 125000,    41667,    125000},
    { 2,  62500,    31250,     62500},
    { 3,  50000,    27778,     50000},
    { 4,  31250,    20833,     31250},
    { 5,  25000,    17857,     25000},
    { 6,  15625,    12500,     15625},
    { 7,  10000,    10000,     11905},
    { 8,   5000,     5000,      5435},
    { 9,   2500,     2500,      2604},
    {10,   1000,     1000,      1016},
    {11,    500,      500,       504},
    {12,  397.5,    397.5,       400},
    {13,    200,      200,    200.64},
    {14,  100.2,    100.2,     100.4},
    {15,  59.98,    59.98,     59.98},
    {16,  49.96,    49.96,     50.00},
    {17,     20,       20,     20.01},
    {18,  16.66,    16.66,     16.66},
    {19,     10,       10,        10},
    {20,      5,        5,         5},
};

static const struct {
    int code;
    double odr;
    double db;
} filt_enh50_pars[] = {
    { 2,  27.27,  47},
    { 3,     25,  62},
    { 5,     20,  85},
    { 6,  16.67,  90},
};




DeviceConfigDialog::DeviceConfigDialog(QWidget *parent, TLTR216 *pmodule, DeviceConfig *defCfg) :
    QDialog(parent),
    ui(new Ui::DeviceConfigDialog), m_pmodule(pmodule), m_cfg(new DeviceConfig(*defCfg)),
    m_cfg_update(false) {
    ui->setupUi(this);


    QString title = tr("Настройки модуля LTR216");
    bool is_opened = LTR216_IsOpened(pmodule) == LTR_OK;

    if (is_opened) {
        QString serial = QSTRING_FROM_CSTR(pmodule->ModuleInfo.Serial);
        if (!serial.isEmpty()) {
            title += QString(", %1").arg(serial);
        }
    }
    setWindowTitle(title);

    ui->sideCfg->layout()->setAlignment(ui->singleChMode, Qt::AlignHCenter | Qt::AlignVCenter);

    QSettings set;
    set.beginGroup("DeviceConfigDialog");
    resize(set.value("WidgetSize", QSize(1000, 600)).toSize());
    m_cfgFilesPath = set.value("LastConfigFile", qApp->applicationDirPath()).toString();
    set.endGroup();


    QToolButton *saveBtn = new QToolButton();
    saveBtn->setDefaultAction(ui->actionConfigSave);
    saveBtn->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
    ui->buttonBox->addButton(saveBtn, QDialogButtonBox::ResetRole);


    QToolButton *loadBtn = new QToolButton();
    loadBtn->setDefaultAction(ui->actionConfigLoad);
    loadBtn->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
    ui->buttonBox->addButton(loadBtn, QDialogButtonBox::ResetRole);

    connect(ui->actionConfigSave, SIGNAL(triggered(bool)), SLOT(saveToFile()));
    connect(ui->actionConfigLoad, SIGNAL(triggered(bool)), SLOT(loadFromFile()));

    ui->multiChannelsCfg->setConfig(m_cfg.data());
    ui->singleCannelCfg->setConfig(m_cfg.data());

    ui->filterType->addItem("SINC5+SINC1", (int)LTR216_FILTER_SINC5_1);
    ui->filterType->addItem("SINC3", (int)LTR216_FILTER_SINC3);
    ui->filterType->addItem("Enhanced 50/60 Hz", (int)LTR216_FILTER_ENH_50HZ);

    QLocale locale;
    for (unsigned i = 0; i < sizeof(filt_sinc5_pars)/sizeof(filt_sinc5_pars[0]); i++) {
        ui->adcOdrSinc5->addItem("",   filt_sinc5_pars[i].code);
    }

    for (unsigned i = 0; i < sizeof(filt_enh50_pars)/sizeof(filt_enh50_pars[0]); i++) {
        ui->adcOdrEnh50->addItem(locale.toString(filt_enh50_pars[i].odr, 'f', 2),   filt_enh50_pars[i].code);
    }

    QList<const DeviceConfig::BgMeasGroup *> groups = DeviceConfig::defaultBgMeasGroups();
    Q_FOREACH(const DeviceConfig::BgMeasGroup *group, groups) {
        ui->bgMeasGroup->addItem(group->name, QVariant::fromValue<const DeviceConfig::BgMeasGroup *>(group));
    }
    ui->bgMeasGroup->addItem(tr("Произвольный набор"), 0);

    showConfig();

    ui->ISrcValue->installEventFilter(this);

    connect(ui->ISrcValue, SIGNAL(valueChanged(double)), this, SLOT(setISrcValue()));

    connect(ui->measTimeBox, SIGNAL(valueChanged(int)), m_cfg.data(), SLOT(setMeasTime(int)));
    connect(ui->swTimeBox, SIGNAL(valueChanged(double)), m_cfg.data(), SLOT(setSwTimeUs(double)));
    connect(ui->adcFreqBox, SIGNAL(valueChangedFromUi(double)), m_cfg.data(), SLOT(setAdcFreq(double)));
    connect(ui->singleChMode, SIGNAL(stateChanged(int)), SLOT(setSignleChMode()));
    connect(ui->filterManualEn, SIGNAL(stateChanged(int)), SLOT(setFilterManualEn()));

    connect(ui->tareEn, SIGNAL(stateChanged(int)), SLOT(setTareEn()));
    connect(ui->ch16ForUrefEn, SIGNAL(stateChanged(int)), SLOT(setCh16ForUref()));


    connect(ui->bgMeasOffsEn, SIGNAL(stateChanged(int)), SLOT(setBgMeasOffs()));
    connect(ui->bgMeasUrefEn, SIGNAL(stateChanged(int)), SLOT(setBgMeasUref()));
    connect(ui->bgMeasUrefOffsEn, SIGNAL(stateChanged(int)), SLOT(setBgMeasUrefOffs()));
    connect(ui->bgMeasVadj, SIGNAL(stateChanged(int)), SLOT(setBgMeasVadj()));
    connect(ui->bgMeasUneg, SIGNAL(stateChanged(int)), SLOT(setBgMeasUneg()));
    connect(ui->bgMeasUx, SIGNAL(stateChanged(int)), SLOT(setBgMeasUx()));
    connect(ui->bgMeasUcm, SIGNAL(stateChanged(int)), SLOT(setBgMeasUcm()));
    connect(ui->bgMeasCheckOpenUneg, SIGNAL(stateChanged(int)), SLOT(setBgMeasCheckOpenUneg()));
    connect(ui->bgMeasCheckOpenUref, SIGNAL(stateChanged(int)), SLOT(setBgMeasCheckOpenUref()));
    connect(ui->bgMeasCheckOpenUx,   SIGNAL(stateChanged(int)), SLOT(setBgMeasCheckOpenUx()));
    connect(ui->bgMeasCheckShortUref, SIGNAL(stateChanged(int)), SLOT(setBgMeasCheckShortUref()));
    connect(ui->bgMeasCheckShortUx,   SIGNAL(stateChanged(int)), SLOT(setBgMeasCheckShortUx()));

    connect(ui->bgMeasGroup, SIGNAL(currentIndexChanged(int)), SLOT(setBgMeasGroup()));
    connect(ui->shortRThresh, SIGNAL(valueChanged(double)), m_cfg.data(), SLOT(setShortCheckRTresh(double)));
    connect(ui->cableLength, SIGNAL(valueChanged(double)), m_cfg.data(), SLOT(setCableLengt(double)));
    connect(ui->cableCapPerUnit, SIGNAL(valueChanged(double)), m_cfg.data(), SLOT(setCableCapPerUnit(double)));


    connect(ui->filterType, SIGNAL(currentIndexChanged(int)), SLOT(setFilterManualParams()));
    connect(ui->adcOdrSinc5, SIGNAL(currentIndexChanged(int)), SLOT(setFilterManualParams()));
    connect(ui->adcOdrSinc3Div, SIGNAL(valueChanged(int)), SLOT(setFilterManualParams()));
    connect(ui->adcOdrEnh50, SIGNAL(currentIndexChanged(int)), SLOT(setFilterManualParams()));

    connect(m_cfg.data(), SIGNAL(adcFreqUpdated(double)), ui->adcFreqBox, SLOT(setValue(double)));
    connect(m_cfg.data(), SIGNAL(filterParamsUpdated()), SLOT(showFilterParams()));
    connect(m_cfg.data(), SIGNAL(frameParamsChanged()), SLOT(showFrameParams()));
    connect(m_cfg.data(), SIGNAL(singleChModeChanged(bool)), SLOT(showSwMode()));


    ui->tareBtn->setEnabled(is_opened);
    if (is_opened)
        connect(ui->tareBtn, SIGNAL(clicked(bool)), SLOT(showTareDialog()));

    connect(ui->showTalbesBtn, SIGNAL(clicked(bool)), SLOT(showTablesDialog()));
}

DeviceConfigDialog::~DeviceConfigDialog() {
    QSettings set;
    set.beginGroup("DeviceConfigDialog");
    set.setValue("WidgetSize", size());
    set.setValue("LastConfigFile", m_cfgFilesPath);
    set.endGroup();
    delete ui;
}


void DeviceConfigDialog::accept() {
    if (!m_cfg->singleChMode() && (m_cfg->swTimeUs() > m_cfg->resultSwTimeUs())) {
        QMessageBox::critical(this, "Ошибка конфигурации", "Невозможно обеспечить установленное время коммутации при данных параметрах");
    } else {
         QDialog::accept();
    }
}

void DeviceConfigDialog::changeEvent(QEvent *e) {
    QDialog::changeEvent(e);
    switch (e->type()) {
        case QEvent::LanguageChange:
            ui->retranslateUi(this);
            break;
        default:
            break;
    }
}

void DeviceConfigDialog::showConfig() {
    int bg_group_idx = -1;
    for (int i = 0; i < ui->bgMeasGroup->count(); i++) {
        const DeviceConfig::BgMeasGroup *group = ui->bgMeasGroup->itemData(i).value<const DeviceConfig::BgMeasGroup *>();
        if (group == m_cfg->bgMeasGroup()) {
            bg_group_idx = i;
        }
    }

    if (bg_group_idx < 0)
        bg_group_idx = ui->bgMeasGroup->count() - 1;

    ui->bgMeasGroup->setCurrentIndex(bg_group_idx);
    updateBgState();

    ui->multiChannelsCfg->showConfig();
    ui->singleCannelCfg->showConfig();
    showSwMode();

    ui->shortRThresh->setValue(m_cfg->shortCheckRThresh());
    ui->cableLength->setValue(m_cfg->cableLength());
    ui->cableCapPerUnit->setValue(m_cfg->cableCapacityPerUnit());
    ui->singleChMode->setChecked(m_cfg->singleChMode());
    ui->ISrcValue->setValue(m_cfg->iSrcValue());
    ui->adcFreqBox->setValue(m_cfg->adcFreq());
    ui->swTimeBox->setValue(m_cfg->swTimeUs());
    ui->measTimeBox->setValue(m_cfg->maesTime());
    ui->filterManualEn->setChecked(m_cfg->filterManualEnabled());
    ui->ch16ForUrefEn->setChecked(m_cfg->ch16ForUref());
    ui->tareEn->setChecked(m_cfg->tareCoefEnabled());
    showFilterParams();
    showFrameParams();

}

void DeviceConfigDialog::showFilterParams() {
    const TLTR216_FILTER_OUT_PARAMS filtParams = m_cfg->filterParams();
    QLocale locale;

    bool manuelEn = m_cfg->filterManualEnabled();
    ui->filterType->setEnabled(manuelEn);
    ui->adcODR->setEnabled(manuelEn);



    setComboBoxItemByData(ui->filterType, filtParams.FilterType);
    ui->adcODR->setCurrentIndex(ui->filterType->currentIndex());
    switch (filtParams.FilterType) {
        case LTR216_FILTER_SINC5_1:
            setComboBoxItemByData(ui->adcOdrSinc5, filtParams.AdcOdrCode);
            break;
        case LTR216_FILTER_SINC3:
            ui->adcOdrSinc3Div->setValue(filtParams.AdcOdrCode);
            ui->adcOdrSinc3Val->setText(locale.toString(filtParams.Odr, 'f', 2));
            break;
        case LTR216_FILTER_ENH_50HZ:
            setComboBoxItemByData(ui->adcOdrEnh50, filtParams.AdcOdrCode);
            break;
    }

    if (filtParams.FilterType == LTR216_FILTER_ENH_50HZ) {
        ui->filterNotch->setText(QString("50/60 Гц, %1 dB").arg(locale.toString(filtParams.NotchDB, 'f', 0)));
    } else {
        ui->filterNotch->setText(QString("%1 Гц").arg(locale.toString(filtParams.NotchFreq, 'f', 2)));
    }
    if (m_cfg->adcSwMode() == LTR216_ADC_SWMODE_MULTICH_SYNC) {
        ui->swResTime->setText(QString("%1 мкс").arg(locale.toString(m_cfg->resultSwTimeUs(), 'f', 3)));
    } else {
        ui->swResTime->setText("");
    }

    ui->adcFreqBox->setEnabled(!(m_cfg->singleChMode() && manuelEn));
}

void DeviceConfigDialog::showFrameParams() {
    m_cfg->fillDeviceConfig(m_pmodule);
    INT err = LTR216_FillRawTables(&m_pmodule->ModuleInfo, &m_pmodule->Cfg, &m_pmodule->RawCfg);
    if (err != LTR_OK) {
        QMessageBox::critical(this, tr("Ошибка"), tr("Ошибка рассчета параметров кадра: %1").arg(QSTRING_FROM_CSTR(LTR216_GetErrorString(err))));
    } else {
        ui->userChCnt->setValue(m_pmodule->RawCfg.UserChCnt);
        ui->rawMainLChCnt->setValue(m_pmodule->RawCfg.MainLChCnt);
        ui->rawBgLChCnt->setValue(m_pmodule->RawCfg.BgLChCnt);
        ui->rawBgStepSize->setValue(m_pmodule->RawCfg.BgStepSize);
        ui->rawFrameSize->setValue(m_pmodule->RawCfg.FrameSize);
        ui->adcChFreq->setValue(m_cfg->adcFreq()/m_pmodule->RawCfg.FrameSize);
    }
}

void DeviceConfigDialog::showSwMode() {
    ui->bgMeasPanel->setEnabled(!m_cfg->singleChMode());
    ui->swTimeBox->setEnabled(!m_cfg->singleChMode());
    ui->chModeStack->setCurrentWidget(m_cfg->singleChMode() ? (QWidget*)ui->singleCannelCfg :
                                                              (QWidget*)ui->multiChannelsCfg);
    if (m_cfg->singleChMode()) {
        ui->singleCannelCfg->updateConfig();
    } else {
        ui->multiChannelsCfg->updateConfig();
    }

    updateBgState();

    QLocale locale;
    for (unsigned i = 0; i < sizeof(filt_sinc5_pars)/sizeof(filt_sinc5_pars[0]); i++) {
        ui->adcOdrSinc5->setItemText(i, locale.toString(m_cfg->singleChMode() ?
                                                            filt_sinc5_pars[i].single_odr :
                                                            filt_sinc5_pars[i].multi_odr,
                                                        'f', 2));
    }
}

void DeviceConfigDialog::setFilterManualEn() {
    bool manual_en = ui->filterManualEn->isChecked();
    m_cfg->setFilterManualEnabled(manual_en);
}

void DeviceConfigDialog::setFilterManualParams() {
    if (m_cfg->filterManualEnabled()) {
        int type = ui->filterType->currentData().toInt();
        int odr_code = 0;
        switch (type) {
            case LTR216_FILTER_SINC5_1:
                odr_code = ui->adcOdrSinc5->currentData().toInt();
                break;
            case LTR216_FILTER_SINC3:
                odr_code = ui->adcOdrSinc3Div->value();
                break;
            case LTR216_FILTER_ENH_50HZ:
                odr_code = ui->adcOdrEnh50->currentData().toInt();
                break;
        }
        m_cfg->setFilterManualParams(type, odr_code);

    }
}

void DeviceConfigDialog::setISrcValue() {
    if (!m_cfg_update) {
        m_cfg->setISrcValue(ui->ISrcValue->value());
        m_cfg_update = true;
        ui->ISrcValue->setValue(m_cfg->iSrcValue());
        m_cfg_update = false;
    }
}

void DeviceConfigDialog::setTareEn() {
    if (!m_cfg_update)
        m_cfg->setTareCoefEn(ui->tareEn->isChecked());
}

void DeviceConfigDialog::setCh16ForUref() {
    if (!m_cfg_update)
        m_cfg->setCh16ForUref(ui->ch16ForUrefEn->isChecked());
}

void DeviceConfigDialog::setBgMeasOffs() {
    if (!m_cfg_update)
        m_cfg->setBgMeasOffs(ui->bgMeasOffsEn->isChecked());
}

void DeviceConfigDialog::setBgMeasUref() {
    if (!m_cfg_update)
        m_cfg->setBgMeasUref(ui->bgMeasUrefEn->isChecked());
}

void DeviceConfigDialog::setBgMeasUrefOffs() {
    if (!m_cfg_update)
        m_cfg->setBgMeasUrefOffs(ui->bgMeasUrefOffsEn->isChecked());
}

void DeviceConfigDialog::setBgMeasVadj() {
    if (!m_cfg_update)
        m_cfg->setBgMeasVadj(ui->bgMeasVadj->isChecked());
}

void DeviceConfigDialog::setBgMeasUneg() {
    if (!m_cfg_update)
        m_cfg->setBgMeasUneg(ui->bgMeasUneg->isChecked());
}

void DeviceConfigDialog::setBgMeasUx() {
    if (!m_cfg_update)
        m_cfg->setBgMeasUx(ui->bgMeasUx->isChecked());
}

void DeviceConfigDialog::setBgMeasUcm() {
    if (!m_cfg_update)
        m_cfg->setBgMeasUcm(ui->bgMeasUcm->isChecked());
}

void DeviceConfigDialog::setBgMeasCheckOpenUneg() {
    if (!m_cfg_update)
        m_cfg->setBgMeasCheckOpenUneg(ui->bgMeasCheckOpenUneg->isChecked());
}

void DeviceConfigDialog::setBgMeasCheckOpenUref() {
    if (!m_cfg_update)
        m_cfg->setBgMeasCheckOpenUref(ui->bgMeasCheckOpenUref->isChecked());
}

void DeviceConfigDialog::setBgMeasCheckOpenUx() {
    if (!m_cfg_update)
        m_cfg->setBgMeasCheckOpenUx(ui->bgMeasCheckOpenUx->isChecked());
}

void DeviceConfigDialog::setBgMeasCheckShortUref() {
    if (!m_cfg_update)
        m_cfg->setBgMeasCheckShortUref(ui->bgMeasCheckShortUref->isChecked());
}

void DeviceConfigDialog::setBgMeasCheckShortUx() {
    if (!m_cfg_update)
        m_cfg->setBgMeasCheckShortUx(ui->bgMeasCheckShortUx->isChecked());
}

void DeviceConfigDialog::setSignleChMode() {
    if (!m_cfg_update) {
        m_cfg->setSingleChMode(ui->singleChMode->isChecked());
    }
}

void DeviceConfigDialog::setBgMeasGroup() {
    const DeviceConfig::BgMeasGroup *group = ui->bgMeasGroup->currentData().value<const DeviceConfig::BgMeasGroup *>();
    m_cfg->setBgMeasGroup(group);
    updateBgState();
}


void DeviceConfigDialog::showTareDialog() {
    showFrameParams();

    TareDialog dlg(this, m_pmodule, m_cfg.data());
    dlg.exec();

    showFrameParams();
}

void DeviceConfigDialog::showTablesDialog() {
    TablesViewDialog dlg(this, m_pmodule);
    dlg.exec();
}



void DeviceConfigDialog::updateBgState() {
    m_cfg_update = true;
    bool manual_update = m_cfg->bgMeasGroup() == 0;

    ui->bgMeasOffsEn->setChecked(m_cfg->bgMeasOffs());
    ui->bgMeasUrefEn->setChecked(m_cfg->bgMeasUref());
    ui->bgMeasUrefOffsEn->setChecked(m_cfg->bgMeasUrefOffs());
    ui->bgMeasVadj->setChecked(m_cfg->bgMeasVadj());
    ui->bgMeasUneg->setChecked(m_cfg->bgMeasUneg());
    ui->bgMeasUx->setChecked(m_cfg->bgMeasUx());
    ui->bgMeasUcm->setChecked(m_cfg->bgMeasUcm());    
    ui->bgMeasCheckOpenUneg->setChecked(m_cfg->bgMeasCheckOpenUneg());
    ui->bgMeasCheckOpenUref->setChecked(m_cfg->bgMeasCheckOpenUref());
    ui->bgMeasCheckOpenUx->setChecked(m_cfg->bgMeasCheckOpenUx());
    ui->bgMeasCheckShortUref->setChecked(m_cfg->bgMeasCheckShortUref());
    ui->bgMeasCheckShortUx->setChecked(m_cfg->bgMeasCheckShortUx());


    ui->bgMeasOffsEn->setEnabled(manual_update);
    ui->bgMeasUrefEn->setEnabled(manual_update);
    ui->bgMeasUrefOffsEn->setEnabled(manual_update);
    ui->bgMeasVadj->setEnabled(manual_update);
    ui->bgMeasUneg->setEnabled(manual_update);
    ui->bgMeasUx->setEnabled(manual_update);
    ui->bgMeasUcm->setEnabled(manual_update);
    ui->bgMeasCheckOpenUneg->setEnabled(manual_update);
    ui->bgMeasCheckOpenUref->setEnabled(manual_update);
    ui->bgMeasCheckOpenUx->setEnabled(manual_update);
    ui->bgMeasCheckShortUref->setEnabled(manual_update);
    ui->bgMeasCheckShortUx->setEnabled(manual_update);

    m_cfg_update = false;
}

QString DeviceConfigDialog::configFileFilter()  {
    return QString("%1 (*%2)").arg(tr("Настройки LTR216")).arg(".lm216cfg");
}


void DeviceConfigDialog::setComboBoxItemByData(QComboBox *box, int data) {
    int idx = 0;
    for (int i=0; i < box->count(); i++) {
        if (box->itemData(i).toInt()==data) {
            idx = i;
            break;
        }
    }
    box->setCurrentIndex(idx);
}


void DeviceConfigDialog::saveToFile() {
    QString title = tr("Сохранение настроек модуля");
    QString filename = QFileDialog::getSaveFileName(this, title, m_cfgFilesPath, configFileFilter());
    if (!filename.isEmpty()) {
        QSettings set(filename, QSettings::IniFormat);

        m_cfg->save(set);
        set.sync();
        if (set.status() == QSettings::NoError) {
            m_cfgFilesPath = filename;
            QMessageBox::information(this, title, tr("Настройки были успешно сохранены в файл"));
        } else {
            QMessageBox::critical(this, title, tr("Ошибка записи настроек в файл"));
        }
    }
}

void DeviceConfigDialog::loadFromFile() {
    QString title = tr("Загрузка конфигурации модуля");
    QString filename = QFileDialog::getOpenFileName(this, title, m_cfgFilesPath, configFileFilter());
    if (!filename.isEmpty()) {
        QSettings set(filename, QSettings::IniFormat);
        if (set.status() != QSettings::NoError) {
            QMessageBox::critical(this, title, tr("Ошибка открытия файла"));
        } else if (!DeviceConfig::hasConfig(set)) {
            QMessageBox::critical(this, title, tr("Неверный формат файла конфигурации"));
        } else {
            m_cfgFilesPath = filename;
            m_cfg->load(set);
            showConfig();
            QMessageBox::information(this, title, tr("Настройки модуля были успешно загружены из файла"));
        }
    }
}

bool DeviceConfigDialog::eventFilter(QObject *obj, QEvent *event) {
#if 0
    if (obj == ui->ISrcValue) {
        if (event->type() == QEvent::KeyPress) {
            QKeyEvent *keyEvent = static_cast<QKeyEvent *>(event);
            if (keyEvent && (keyEvent->key() == Qt::Key_Return)) {
                keyEvent->accept();
            }
        }
    }
#endif
    return QObject::eventFilter(obj, event);
}

void DeviceConfigDialog::keyPressEvent(QKeyEvent *event) {
    QWidget::keyPressEvent(event);
    /* чтобы по вводу только был принят текст и обновлено значение, но событие
       не передавалось дальше */
    if (event->key() == Qt::Key_Return) {
        event->accept();
    }
}

