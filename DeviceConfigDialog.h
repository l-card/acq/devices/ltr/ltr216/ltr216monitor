#ifndef DEVICECONFIGDIALOG_H
#define DEVICECONFIGDIALOG_H

#include <QDialog>
#include "ltr/include/ltr216api.h"
#include "DeviceConfig.h"
class QComboBox;
class QTableWidgetItem;
class QTableWidget;

namespace Ui {
class DeviceConfigDialog;
}

class DeviceConfigDialog : public QDialog {
    Q_OBJECT

public:
    explicit DeviceConfigDialog(QWidget *parent, TLTR216 *pmodule, DeviceConfig *defCfg);
    ~DeviceConfigDialog();

    QSharedPointer<DeviceConfig> config() const {return m_cfg;}
public Q_SLOTS:
    void accept();

protected:
    void changeEvent(QEvent *e);

private Q_SLOTS:
    void showConfig();
    void showFilterParams();
    void showFrameParams();
    void showSwMode();
    void setFilterManualEn();
    void setFilterManualParams();

    void setISrcValue();
    void setTareEn();
    void setCh16ForUref();
    void setBgMeasOffs();
    void setBgMeasUref();
    void setBgMeasUrefOffs();
    void setBgMeasVadj();
    void setBgMeasUneg();
    void setBgMeasUx();
    void setBgMeasUcm();
    void setBgMeasCheckOpenUneg();
    void setBgMeasCheckOpenUref();
    void setBgMeasCheckOpenUx();
    void setBgMeasCheckShortUref();
    void setBgMeasCheckShortUx();

    void setSignleChMode();
    void setBgMeasGroup();

    void showTareDialog();
    void showTablesDialog();


    void saveToFile();
    void loadFromFile();
protected:
    bool eventFilter(QObject *obj, QEvent *event);
    void keyPressEvent(QKeyEvent *event);
private:
    void updateBgState();
    static QString configFileFilter();

    Ui::DeviceConfigDialog *ui;
    TLTR216 *m_pmodule;
    QSharedPointer<DeviceConfig> m_cfg;
    bool m_cfg_update;
    QString m_cfgFilesPath;

    void setComboBoxItemByData(QComboBox *box, int data);
};

#endif // DEVICECONFIGDIALOG_H
