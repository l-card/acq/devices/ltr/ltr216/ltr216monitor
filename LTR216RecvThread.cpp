#include "LTR216RecvThread.h"
#include <QScopedPointer>


static double max_vals[] = {35, 70, 1200};



LTR216RecvThread::LTR216RecvThread(TLTR216 *pmodule, unsigned recv_time) : m_pmodule(pmodule), m_recv_time_ms(recv_time), m_stop_req(false), m_last_err(-1) {
    qRegisterMetaType<QSharedPointer<LTR216RecvThread::Frame> >("QSharedPointer<LTR216RecvThread::Frame>");
}

void LTR216RecvThread::run() {
    INT err = LTR216_InitMeasParams(m_pmodule, LTR216_INIT_MEAS_ALL, 0, NULL);

    err = LTR216_Start(m_pmodule);
    if (err == LTR_OK) {
        unsigned ch_cnt = m_pmodule->Cfg.LChCnt;
        unsigned recv_frames_cnt = (m_recv_time_ms * m_pmodule->State.FrameFreq / 1000);
        unsigned recv_samples_cnt = recv_frames_cnt * ch_cnt;
        if (recv_frames_cnt < 1) {
            recv_frames_cnt = 1;
            m_recv_time_ms = 1000. * recv_frames_cnt / m_pmodule->State.FrameFreq + 0.5;
        }

        unsigned recv_tout = 1000 +  m_recv_time_ms;
        unsigned recv_wrds_cnt = m_pmodule->State.FrameWordsCount * recv_frames_cnt;
        QScopedArrayPointer<DWORD> words(new DWORD[recv_wrds_cnt]);
        QScopedArrayPointer<double> vals(new double[recv_samples_cnt]);
        QScopedArrayPointer<TLTR216_DATA_CH_STATUS> ch_status(new TLTR216_DATA_CH_STATUS[ch_cnt]);

        unsigned idx = 0;

        while ((err == LTR_OK) && !m_stop_req) {
            INT cur_recv_wrds_cnt = 0;
            while ((err == LTR_OK) && !m_stop_req && (cur_recv_wrds_cnt != recv_wrds_cnt)) {
                INT recv_size = LTR216_Recv(m_pmodule, &words.data()[cur_recv_wrds_cnt], NULL, recv_wrds_cnt - cur_recv_wrds_cnt, recv_tout);
                if (recv_size < 0) {
                    err = recv_size;
                } else {
                    cur_recv_wrds_cnt += recv_size;
                }
            }

            if ((err == LTR_OK) && (cur_recv_wrds_cnt == recv_wrds_cnt)) {
                INT proc_size = recv_wrds_cnt;
                QSharedPointer<Frame> frame(new Frame());



                err = LTR216_ProcessData(m_pmodule, words.data(), vals.data(), &proc_size,
                                         0, &frame->status, ch_status.data());
                if (err == LTR_OK) {                    
                    for (unsigned ch_num = 0; ch_num < ch_cnt; ch_num++) {
                        QSharedPointer<LTR216RecvThread::Data> chData(new LTR216RecvThread::Data());
                        chData->dt = 1./m_pmodule->State.FrameFreq;
                        chData->x_idx = idx;
                        chData->max_value = max_vals[m_pmodule->Cfg.LChTbl[ch_num].Range];
                        chData->data.reserve(recv_frames_cnt);
                        for (unsigned i = 0; i < recv_frames_cnt; i++) {
                            chData->data.append(1000. * vals[ch_cnt*i + ch_num]);
                        }
                        chData->status = ch_status[ch_num];

                        frame->chData.append(chData);
                    }
                    idx += recv_frames_cnt;
                    Q_EMIT dataReceived(frame);
                }
            }
        }


        INT stop_err = LTR216_Stop(m_pmodule);
        if (err == LTR_OK)
            err = stop_err;
    }

    m_last_err = err;
}
