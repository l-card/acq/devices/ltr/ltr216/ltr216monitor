#ifndef LTR216RECVTHREAD_H
#define LTR216RECVTHREAD_H

#include <QThread>
#include <QVector>
#include <QList>
#include <QSharedPointer>
#include "ltr/include/ltr216api.h"

class LTR216RecvThread : public QThread {
    Q_OBJECT
public:
    struct Data {
    public:
        quint64 x_idx; /* номер индекса для первого отсчета блока */
        double  dt;  /* время между отсчетами канала */
        double  max_value;
        TLTR216_DATA_CH_STATUS status;
        QVector<double> data;
    };

    struct Frame {
        TLTR216_DATA_STATUS status;
        QList<QSharedPointer<LTR216RecvThread::Data> > chData;
    };

    LTR216RecvThread(TLTR216 *pmodule, unsigned recv_time);

    void stopRequest() {m_stop_req = true;}
    INT lastErrorCode() const {return m_last_err;}


Q_SIGNALS:
    void dataReceived(QSharedPointer<LTR216RecvThread::Frame> frame);
protected:
    void run();
private:
    TLTR216 *m_pmodule;
    volatile INT m_last_err;
    unsigned m_recv_time_ms;
    volatile bool m_stop_req;
};

#endif // LTR216RECVTHREAD_H
