#include "MainWindow.h"
#include "ui_MainWindow.h"
#include <QSettings>
#include <QMessageBox>
#include "DeviceConfigDialog.h"
#include "lqtdefs.h"
#include "MesValStateWidget.h"
#include "HierarchicalHeaderView.h"
#include "actions/TableCopyAction.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    m_cfg(new DeviceConfig()),
    m_recvThread(0), m_stop_req(false) {

    ui->setupUi(this);

    setStyleSheet("QGroupBox{border:1px solid black;margin-top: 0.5em;}"
                  "QGroupBox::title{subcontrol-origin: margin; subcontrol-position:top left;padding:0 3px 0 3px; left: 10px;}");


    LTR216_Init(&hmodule);

    QSettings set;
    set.beginGroup("MainWindow");
    if (!set.contains("state")) {
        setWindowState(Qt::WindowMaximized);
    } else {
        restoreState(set.value("state").toByteArray());
    }
    resize(set.value("size", QSize(700, 700)).toSize());
    move(set.value("pos", QPoint(200, 100)).toPoint());

    if (set.contains("ChSplitState")) {
        ui->splitter->restoreState(set.value("ChSplitState").toByteArray());
    } else {
        ui->splitter->setSizes(QList<int>() << 700 << 200);
    }
    set.endGroup();

    HierarchicalHeaderView* hv=new HierarchicalHeaderView(Qt::Horizontal, this);

    int top_hdr_pos = 0;
    QStandardItemModel *headerModel = new QStandardItemModel();
    QStandardItem *item = new QStandardItem(tr("№\nканала"));
    headerModel->setItem(0, top_hdr_pos++, item);

    item = new QStandardItem(tr("Разбаланс"));
    item->appendColumn(QList<QStandardItem*>() << new QStandardItem(tr("DC, мВ")));
    item->appendColumn(QList<QStandardItem*>() << new QStandardItem(tr("AC, мВ")));
    headerModel->setItem(0, top_hdr_pos++, item);

    item = new QStandardItem(tr("Диа-\nпазон"));
    headerModel->setItem(0, top_hdr_pos++, item);

    item = new QStandardItem(tr("Линия"));
    item->appendColumn(QList<QStandardItem*>() << new QStandardItem(tr("КЗ")));
    item->appendColumn(QList<QStandardItem*>() << new QStandardItem(tr("Обр.")));
    headerModel->setItem(0, top_hdr_pos++, item);

    item = new QStandardItem(tr("Условия"));
    item->appendColumn(QList<QStandardItem*>() << new QStandardItem("Udiff"));
    item->appendColumn(QList<QStandardItem*>() << new QStandardItem("Ucm"));
    headerModel->setItem(0, top_hdr_pos++, item);

    item = new QStandardItem(tr("Промежуточные измерения"));
    item->appendColumn(QList<QStandardItem*>() << new QStandardItem("Ux, В"));
    item->appendColumn(QList<QStandardItem*>() << new QStandardItem("Udiff, В"));
    item->appendColumn(QList<QStandardItem*>() << new QStandardItem("Ucm, В"));
    headerModel->setItem(0, top_hdr_pos++, item);

    item = new QStandardItem(tr("Рабочие границы"));
    item->appendColumn(QList<QStandardItem*>() << new QStandardItem("Ucm min, В"));
    item->appendColumn(QList<QStandardItem*>() << new QStandardItem("Ucm max, В"));
    headerModel->setItem(0, top_hdr_pos++, item);

    item = new QStandardItem(tr("Характеристики синусоидального сигнала"));
    item->appendColumn(QList<QStandardItem*>() << new QStandardItem("Част. пика"));
    item->appendColumn(QList<QStandardItem*>() << new QStandardItem("Амп. пика"));
    item->appendColumn(QList<QStandardItem*>() << new QStandardItem("SNR"));
    item->appendColumn(QList<QStandardItem*>() << new QStandardItem("SINAD"));
    item->appendColumn(QList<QStandardItem*>() << new QStandardItem("ENOB"));
    headerModel->setItem(0, top_hdr_pos++, item);
    hv->setHeaderModel(headerModel);
    hv->setSectionsClickable(true);
    ui->paramsTable->setHorizontalHeader(hv);
    ui->paramsTable->setColumnCount(18);
    ui->paramsTable->addAction(new TableCopyAction(ui->paramsTable));


    PlotConfigDialog::Config cfg;
    cfg.x.min = 0;
    cfg.x.max = 1000;
    cfg.x.autoScale = true;
    cfg.y.min = 0;
    cfg.y.max = 25;
    cfg.y.autoScale = true;
    ui->timePlot->loadConfig(set, "TimePlot", &cfg);


    ui->timePlot->xAxis->setLabel(tr("Время, мс"));
    ui->timePlot->yAxis->setLabel(tr("Амплитуда, мВ"));

    cfg.x.autoScale = true;
    cfg.x.min = 0;
    cfg.x.max = 50*1000;
    cfg.y.autoScale = false;
    cfg.y.min = -150;
    cfg.y.max = 0;

    ui->fftPlot->loadConfig(set, "FftPlot", &cfg);

    ui->fftPlot->xAxis->setLabel(tr("Частота, Гц"));
    ui->fftPlot->yAxis->setLabel(tr("Спектр амплитудный, dB"));

    ui->uneg->setShortVisible(false);
    ui->vadj->setOpenVisible(false);
    ui->vadj->setShortVisible(false);

    m_cfg->load(set);
    updateConfigUi();

    ui->actionClearSamples->setEnabled(false);

    ui->refreshDevsBtn->setDefaultAction(ui->actionRefreshDeviceList);

    connect(ui->actionConfigure, SIGNAL(triggered(bool)), SLOT(configure()));
    connect(ui->actionClearSamples, SIGNAL(triggered(bool)), SLOT(clearData()));
    connect(ui->actionRefreshDeviceList, SIGNAL(triggered(bool)), SLOT(refreshDevList()));

    connect(ui->paramsTable, SIGNAL(itemChanged(QTableWidgetItem*)),
            SLOT(onParamTableItemChanged(QTableWidgetItem*)));

    connect(&m_proc, SIGNAL(newBlock(QSharedPointer<DataBlockProcessor::DataBlock>)),
            SLOT(showBlock(QSharedPointer<DataBlockProcessor::DataBlock>)));
    connect(this, SIGNAL(procParamsUpdated(int)), &m_proc, SLOT(processParamsUpdate(int)));

    m_procThread.start();
    m_proc.moveToThread(&m_procThread);

    refreshDevList();

}

MainWindow::~MainWindow() {
    delete ui;
}

void MainWindow::changeEvent(QEvent *e) {
    QMainWindow::changeEvent(e);
    switch (e->type()) {
        case QEvent::LanguageChange:
            ui->retranslateUi(this);
            break;
        default:
            break;
    }
}

void MainWindow::closeEvent(QCloseEvent *event) {
    QSettings set;
    set.beginGroup("MainWindow");
    /* если коно в настоящий момент не свернуто - то сохраняем его текущие
       параметры */
    if (!isMinimized()) {
        set.setValue("state", saveState());
        set.setValue("size", size());
        set.setValue("pos", pos());
    }

    set.beginGroup("Channels");
    for (int ch = 0; ch < m_chUiList.size(); ch++) {
        saveCh(set, ch);
    }
    set.endGroup();

    set.endGroup();

    ui->timePlot->saveConfig(set);
    ui->fftPlot->saveConfig(set);

    m_cfg->save(set);

    closeModule();

    m_procThread.quit();
    m_procThread.wait();
}

void MainWindow::updateUiState() {    
    bool opened = LTR216_IsOpened(&hmodule) == LTR_OK;
    ui->moduleLed->setChecked(opened);

    if (opened) {
        QLocale locale;
        ui->isrcSet->setText(locale.toString(m_cfg->iSrcValue(), 'f', 3));
    } else {
        ui->isrcSet->setText("");
    }

}


void MainWindow::showError(QString msg, INT err) {
    QMessageBox::critical(this, tr("Ошибка"), tr("%1. Ошибка %2: %3").arg(msg).arg(err).arg(QSTRING_FROM_CSTR(LTR216_GetErrorString(err))));
}

void MainWindow::configure() {
    stopDevice();
    DeviceConfigDialog dlg(this, &hmodule, m_cfg.data());
    if (dlg.exec() == QDialog::Accepted) {
        m_cfg = dlg.config();
        updateConfigUi();
    }
    startDevice();
}

void MainWindow::showBlock(QSharedPointer<DataBlockProcessor::DataBlock> block) {
    QLocale locale;
    for (int ch_num = 0; (ch_num < block->chData.size()) && (ch_num < m_chUiList.size()); ch_num++) {
        ChUi *chUi = m_chUiList.at(ch_num);
        QSharedPointer<DataBlockProcessor::ChDataBlock> chData = block->chData.at(ch_num);

        chUi->ovStatus->setState(chData->status.StatusFlags & LTR216_CH_STATUS_FLAG_OVERRANGE ?
                                  CondLedIndicator::StateBad : CondLedIndicator::StateOk);
        chUi->timeGraph->setData(chData->x_vals, chData->amp_vals, true);
        ui->timePlot->updateAxisAutoScale();
        ui->timePlot->requestRelpot();

        if (!chData->fft_db.isEmpty()) {
            chUi->fftGraph->setData(chData->fft_x_vals, chData->fft_db, true);
        }
        ui->fftPlot->updateAxisAutoScale();
        ui->fftPlot->requestRelpot();




        chUi->itemDC->setText(locale.toString(chData->dc, 'f', 6));
        chUi->itemAC->setText(locale.toString(chData->ac, 'f', 6));

        if (m_cfg->bgMeasCheckShortUx()) {
            setMeasLedVal(chUi->uxShortStatus,
                          chData->status.Ux.Status == LTR216_MEASSTATUS_OPEN
                          ? LTR216_MEASSTATUS_CANT_CALC : chData->status.Ux.Status,
                          chData->status.Ux.Status != LTR216_MEASSTATUS_SHORT);
        }
        if (m_cfg->bgMeasCheckOpenUx()) {
            setMeasLedVal(chUi->uxOpenStatus, chData->status.Ux.Status, chData->status.Ux.Status != LTR216_MEASSTATUS_OPEN);
        }

        if (m_cfg->bgMeasIsUxUsed()) {
            setMeasTableVal(chUi->itemUabs, &chData->status.Ux, true, 6);
            double udiff = chData->status.Ux.Value - block->status.Uref.Value;
            bool udif_ok_range = qAbs(udiff*1000) < 3*chData->range_value;

            TLTR216_MEASSTATE udiffState = chData->status.Ux;
            udiffState.Value = udiff;
            if ((chData->status.Ux.Status != LTR216_MEASSTATUS_NOT_USED) &&
                    (chData->status.Ux.Status != LTR216_MEASSTATUS_NOT_INIT)) {
                if ((chData->status.Ux.Status != LTR216_MEASSTATUS_OK) ||
                        (block->status.Uref.Status != LTR216_MEASSTATUS_OK)) {
                    udiffState.Status = LTR216_MEASSTATUS_CANT_CALC;
                    udiffState.ValueValid = FALSE;
                }

            }

            setMeasTableVal(chUi->itemUdiff, &udiffState, udif_ok_range, 6);
            setMeasLedVal(chUi->udStatus, udiffState.Status, udif_ok_range);

            if (m_cfg->bgMeasUcm()) {
                setMeasTableVal(chUi->itemUcm, &chData->status.Ucm, true, 6);

                udiffState.Value = chData->status.UcmMin;
                setMeasTableVal(chUi->itemUcmMin, &udiffState, true, 6);
                udiffState.Value = chData->status.UcmMax;
                setMeasTableVal(chUi->itemUcmMax, &udiffState, true, 6);

                setMeasLedVal(chUi->ucmStatus, chData->status.Ucm.Status,
                              chData->status.Ucm.Status != LTR216_MEASSTATUS_BAD_VALUE_RANGE);
            }
        }


        if (chData->peak.valid) {
            chUi->itemPeakFreq->setText(locale.toString(chData->peak.freq, 'f', 3));
            chUi->itemPeakAmp->setText(locale.toString(chData->peak.amp, 'f', 6));
        } else {
            chUi->itemPeakFreq->setText("");
            chUi->itemPeakAmp->setText("");
        }

        if (chData->spectrum_params.valid) {
            chUi->itemSNR->setText(locale.toString(chData->spectrum_params.snr, 'f', 2));
            chUi->itemSINAD->setText(locale.toString(chData->spectrum_params.sinad, 'f', 2));
            chUi->itemENOB->setText(locale.toString(chData->spectrum_params.enob, 'f', 2));
        } else {
            chUi->itemSNR->setText("");
            chUi->itemSINAD->setText("");
            chUi->itemENOB->setText("");
        }
    }

    ui->uref->setVal(&block->status.Uref, true);
    if (block->status.Uref.Status == LTR216_MEASSTATUS_OK) {
        double ival = m_cfg->iSrcValue()/1000.;
        double rref = block->status.Uref.Value /ival;
        double rrefPow  = ival * block->status.Uref.Value;
        QLocale locale;
        ui->rref->setText(locale.toString(rref, 'f', 4));
        ui->rrefPow->setText(locale.toString(rrefPow, 'f', 4));


        double irec;
        double ucm = block->status.Uref.Value + block->status.Uneg.Value;
        double rcm = ucm / ival;
        if (rcm >= 250) {
            irec = 3.5 /rcm;
        } else if (rcm >= 100) {
            irec = 2.5 /rcm;
        } else {
            irec = 2.5 / rcm;
            double irec_max = sqrt(1.1/(rcm * (m_cfg->ch16ForUref() ? 16 : 17)));
            if (irec > irec_max)
                irec = irec_max;
            if (irec > 0.03)
                irec = 0.03;
        }


        ui->isrcRec->setText(locale.toString(1000*irec, 'f', 3));
    } else {
        ui->rref->clear();
        ui->rrefPow->clear();
        ui->isrcRec->clear();
    }

    bool en = block->status.UrefR.Status != LTR216_MEASSTATUS_NOT_USED;
    ui->uref_r->setVal(&block->status.UrefR, true);
    ui->rref_r->setEnabled(en);
    ui->pref_r->setEnabled(en);
    if (en && (block->status.UrefR.Status == LTR216_MEASSTATUS_OK)) {
        double ival = m_cfg->iSrcValue()/1000;
        double rref = block->status.UrefR.Value/ival;
        double pref  = ival * block->status.UrefR.Value;
        QLocale locale;
        ui->rref_r->setText(locale.toString(rref, 'f', 3));
        ui->pref_r->setText(locale.toString(pref, 'f', 3));
    } else {
        ui->rref_r->clear();
        ui->pref_r->clear();
    }


    ui->vadj->setVal(&block->status.Vadj, true);

    TLTR216_MEASSTATE neg_state = block->status.Uneg;
    neg_state.Value *= 1000;
    ui->uneg->setVal(&neg_state, true);

    ui->loadPowExcLed->setState((block->status.StatusFlags & LTR216_STATUS_FLAG_LOAD_POW_EXCEEDED) ?
                                    CondLedIndicator::StateBad : CondLedIndicator::StateOk);

    ui->actionClearSamples->setEnabled(true);
}

void MainWindow::clearData() {
    ui->uref->setShortEnabled(m_cfg->bgMeasCheckShortUref());
    ui->uref->setOpenEnabled(m_cfg->bgMeasCheckOpenUref());
    ui->uref->setMeasEnabled(m_cfg->bgMeasUref());

    ui->uref_r->setShortEnabled(m_cfg->bgMeasCheckShortUref() && m_cfg->ch16ForUref());
    ui->uref_r->setOpenEnabled(m_cfg->bgMeasCheckOpenUref() && m_cfg->ch16ForUref());
    ui->uref_r->setMeasEnabled(m_cfg->bgMeasUref() && m_cfg->ch16ForUref());
    ui->rref_r->setEnabled(m_cfg->bgMeasUref() && m_cfg->ch16ForUref());
    ui->pref_r->setEnabled(m_cfg->bgMeasUref() && m_cfg->ch16ForUref());

    ui->uneg->setMeasEnabled(m_cfg->bgMeasUneg());
    ui->uneg->setOpenEnabled(m_cfg->bgMeasCheckOpenUneg());

    ui->vadj->setMeasEnabled(m_cfg->bgMeasVadj());

    Q_FOREACH(ChUi *chUi, m_chUiList) {
        chUi->timeGraph->data()->clear();
        chUi->fftGraph->data()->clear();

        chUi->ovStatus->setState(CondLedIndicator::StateOff);
        chUi->uxShortStatus->setState(m_cfg->bgMeasCheckShortUx() ? CondLedIndicator::StateOff : CondLedIndicator::StateDisabled);
        chUi->uxOpenStatus->setState(m_cfg->bgMeasCheckOpenUx() ? CondLedIndicator::StateOff : CondLedIndicator::StateDisabled);
        chUi->udStatus->setState(m_cfg->bgMeasIsUxUsed() ? CondLedIndicator::StateOff : CondLedIndicator::StateDisabled);
        chUi->ucmStatus->setState(m_cfg->bgMeasUcm() ? CondLedIndicator::StateOff : CondLedIndicator::StateDisabled);

        initMeasTableVal(chUi->itemDC, true);
        initMeasTableVal(chUi->itemAC, true);
        initMeasTableVal(chUi->itemUabs, m_cfg->bgMeasIsUxUsed());
        initMeasTableVal(chUi->itemUdiff, m_cfg->bgMeasIsUxUsed());
        initMeasTableVal(chUi->itemUcm, m_cfg->bgMeasUcm());
        initMeasTableVal(chUi->itemUcmMin, m_cfg->bgMeasUcm());
        initMeasTableVal(chUi->itemUcmMax, m_cfg->bgMeasUcm());
        initMeasTableVal(chUi->itemPeakFreq, true);
        initMeasTableVal(chUi->itemPeakAmp, true);
        initMeasTableVal(chUi->itemSNR, true);
        initMeasTableVal(chUi->itemSINAD, true);
        initMeasTableVal(chUi->itemENOB, true);
    }

    ui->timePlot->replot();
    ui->fftPlot->replot();

    ui->loadPowExcLed->setState(CondLedIndicator::StateOff);
    ui->uref->clear();
    ui->uref_r->clear();
    ui->vadj->clear();
    ui->uneg->clear();
    ui->rref->clear();
    ui->rref_r->clear();
    ui->pref_r->clear();
    ui->rrefPow->clear();
    ui->isrcRec->clear();


    ui->actionClearSamples->setEnabled(false);
}

void MainWindow::onParamTableItemChanged(QTableWidgetItem *item) {
    for (int i = 0; i < m_chUiList.size(); i++) {
        ChUi *chUi = m_chUiList.at(i);
        if (item == chUi->itemCh) {
            bool visible = chUi->itemCh->checkState() == Qt::Checked;
            chUi->fftGraph->setVisible(visible);
            chUi->timeGraph->setVisible(visible);
        }
    }

    ui->timePlot->updateAxisAutoScale();
    ui->timePlot->requestRelpot();
    ui->fftPlot->updateAxisAutoScale();
    ui->fftPlot->requestRelpot();
}

void MainWindow::startDevice() {
    if (LTR216_IsOpened(&hmodule) == LTR_OK) {
        m_cfg->fillDeviceConfig(&hmodule);
        INT err = LTR216_SetADC(&hmodule);
        if (err != LTR_OK) {
            showError("Не удалось записать конфигурацию модуля", err);
        } else {
            if (m_cfg->enabledChannelsCnt() > 0) {
                unsigned recv_time = m_cfg->maesTime();
                clearData();

                //updateProcParams();

                m_recvThread = new LTR216RecvThread(&hmodule, recv_time);
                connect(m_recvThread, SIGNAL(finished()), SLOT(onRecvThreadFinished()));
                connect(m_recvThread, SIGNAL(dataReceived(QSharedPointer<LTR216RecvThread::Frame>)),
                        &m_proc, SLOT(processFrame(QSharedPointer<LTR216RecvThread::Frame>)));
                m_recvThread->start();
            }
        }
    }
    updateUiState();
}

void MainWindow::stopDeviceReq() {
    if (m_recvThread) {
        disconnect(m_recvThread, SIGNAL(dataReceived(QSharedPointer<LTR216RecvThread::Frame>)),
                &m_proc, SLOT(processFrame(QSharedPointer<LTR216RecvThread::Frame>)));
        m_stop_req = true;
        m_recvThread->stopRequest();
    }
}

void MainWindow::stopDevice() {
    if (m_recvThread) {
        stopDeviceReq();
        m_recvThread->wait();
        m_recvThread = 0;
        updateUiState();
    }
}

void MainWindow::onRecvThreadFinished() {
    m_stop_req = false;
    LTR216RecvThread *thread = qobject_cast<LTR216RecvThread *>(sender());
    if (thread) {
        if (thread->lastErrorCode() != LTR_OK) {
            showError("Ошибка приема данных", thread->lastErrorCode());
        }

        if (!m_stop_req && (thread == m_recvThread)) {
            m_recvThread = 0;
            updateUiState();
        }
        delete thread;
    }
}

bool MainWindow::running() {
    return m_recvThread;
}


void MainWindow::updateConfigUi() {
    QSettings set;
    set.beginGroup("MainWindow");
    set.beginGroup("Channels");
    int new_ch_cnt = m_cfg->enabledChannelsCnt();
    while (new_ch_cnt > m_chUiList.size())
        addCh(set);
    while (new_ch_cnt < m_chUiList.size())
        removeCh(set);

    int cur_lch_pos = 0;
    for (int ch = 0; ch < LTR216_CHANNELS_CNT; ch++) {
        if (m_cfg->channelEnabled(ch)) {
            m_chUiList.at(cur_lch_pos++)->itemCh->setText(QString::number(ch+1));
        }
    }
    set.endGroup();
    set.endGroup();
}

void MainWindow::addCh(QSettings &set) {
    int num = m_chUiList.size();
    static QColor colors[] = {Qt::blue, Qt::red, Qt::darkGreen, Qt::magenta,
                              Qt::darkCyan, Qt::darkYellow, Qt::darkGray, Qt::black};
    QColor ch_color = colors[num % (sizeof(colors)/sizeof(colors[0]))];

    ChUi *chUi = new ChUi;
    chUi->timeGraph = ui->timePlot->addGraph();
    chUi->timeGraph->setPen(QPen(ch_color));
    chUi->timeGraph->setAdaptiveSampling(false);

    chUi->fftGraph = ui->fftPlot->addGraph();
    chUi->fftGraph->setPen(QPen(ch_color));
    chUi->fftGraph->setAdaptiveSampling(false);

    ui->paramsTable->setRowCount(num + 1);
    int col = 0;
    ui->paramsTable->setColumnWidth(col, 60);
    ui->paramsTable->setItem(num, col++, chUi->itemCh = new QTableWidgetItem());
    ui->paramsTable->setItem(num, col++, chUi->itemDC = new QTableWidgetItem());
    ui->paramsTable->setItem(num, col++, chUi->itemAC = new QTableWidgetItem());

    int led_col_width = 55;
    ui->paramsTable->setColumnWidth(col, led_col_width);
    ui->paramsTable->setCellWidget(num, col++, chUi->ovStatus = new CondLedIndicator());
    ui->paramsTable->setColumnWidth(col, led_col_width);
    ui->paramsTable->setCellWidget(num, col++, chUi->uxShortStatus = new CondLedIndicator());
    ui->paramsTable->setColumnWidth(col, led_col_width);
    ui->paramsTable->setCellWidget(num, col++, chUi->uxOpenStatus = new CondLedIndicator());

    ui->paramsTable->setColumnWidth(col, led_col_width);
    ui->paramsTable->setCellWidget(num, col++, chUi->udStatus = new CondLedIndicator());
    ui->paramsTable->setColumnWidth(col, led_col_width);
    ui->paramsTable->setCellWidget(num, col++, chUi->ucmStatus = new CondLedIndicator());

    ui->paramsTable->setItem(num, col++, chUi->itemUabs = new QTableWidgetItem());
    ui->paramsTable->setItem(num, col++, chUi->itemUdiff = new QTableWidgetItem());
    ui->paramsTable->setItem(num, col++, chUi->itemUcm = new QTableWidgetItem());
    ui->paramsTable->setItem(num, col++, chUi->itemUcmMin = new QTableWidgetItem());
    ui->paramsTable->setItem(num, col++, chUi->itemUcmMax = new QTableWidgetItem());
    ui->paramsTable->setItem(num, col++, chUi->itemPeakFreq = new QTableWidgetItem());
    ui->paramsTable->setItem(num, col++, chUi->itemPeakAmp = new QTableWidgetItem());
    ui->paramsTable->setItem(num, col++, chUi->itemSNR = new QTableWidgetItem());
    ui->paramsTable->setItem(num, col++, chUi->itemSINAD = new QTableWidgetItem());
    ui->paramsTable->setItem(num, col++, chUi->itemENOB = new QTableWidgetItem());

    chUi->measItemList.append(chUi->itemDC);
    chUi->measItemList.append(chUi->itemAC);
    chUi->measItemList.append(chUi->itemUabs);
    chUi->measItemList.append(chUi->itemUdiff);
    chUi->measItemList.append(chUi->itemUcm);
    chUi->measItemList.append(chUi->itemUcmMin);
    chUi->measItemList.append(chUi->itemUcmMax);
    chUi->measItemList.append(chUi->itemPeakFreq);
    chUi->measItemList.append(chUi->itemPeakAmp);
    chUi->measItemList.append(chUi->itemSNR);
    chUi->measItemList.append(chUi->itemSINAD);
    chUi->measItemList.append(chUi->itemENOB);

    set.beginGroup(QString("Ch%1").arg(num));
    bool visible = set.value("Visible", true).toBool();

    set.endGroup();

    chUi->itemCh->setForeground(ch_color);
    chUi->itemCh->setCheckState(visible ? Qt::Checked : Qt::Unchecked);
    chUi->timeGraph->setVisible(visible);
    chUi->fftGraph->setVisible(visible);

    m_chUiList.append(chUi);
}

void MainWindow::removeCh(QSettings &set) {
    int num = m_chUiList.size() - 1;
    saveCh(set, num);


    ChUi *chUi = m_chUiList.at(num);
    ui->paramsTable->removeRow(num);
    ui->timePlot->removeGraph(chUi->timeGraph);
    ui->fftPlot->removeGraph(chUi->fftGraph);
    m_chUiList.removeLast();
}

void MainWindow::saveCh(QSettings &set, int num) {
    set.beginGroup(QString("Ch%1").arg(num));
    set.setValue("Visible",  m_chUiList.at(num)->itemCh->checkState() == Qt::Checked);
    set.endGroup();
}

void MainWindow::setMeasTableVal(QTableWidgetItem *item, const TLTR216_MEASSTATE *state, bool ok, int prec) {
    QLocale locale;
    if (((state->Status == LTR216_MEASSTATUS_OK) && ok) || (state->Status == LTR216_MEASSTATUS_NOT_INIT)) {
        item->setForeground(QBrush());
        item->setBackground(QBrush());
    } else if ((state->Status == LTR216_MEASSTATUS_NOT_USED) ) {
        item->setForeground(QBrush());
        item->setBackground(QBrush(Qt::darkGray));
    } else {
        item->setForeground(QBrush(Qt::red));
        item->setBackground(QBrush());
    }

    if (state->ValueValid) {
        item->setText(locale.toString(state->Value, 'f', prec));
    } else {
        item->setText("");
    }
}

void MainWindow::initMeasTableVal(QTableWidgetItem *item, bool enabled) {
    if (enabled) {
        item->setForeground(QBrush());
        item->setBackground(QBrush());
    } else {
        item->setForeground(QBrush());
        item->setBackground(QBrush(Qt::darkGray));
    }
    item->setText("");
}

void MainWindow::setMeasLedVal(CondLedIndicator *led, int status, bool ok) {
    if (status == LTR216_MEASSTATUS_NOT_USED) {
        led->setState(CondLedIndicator::StateDisabled);    
    } else if (status == LTR216_MEASSTATUS_NOT_INIT) {
        led->setState(CondLedIndicator::StateOff);
    } else if (status == LTR216_MEASSTATUS_CANT_CALC) {
        led->setState(CondLedIndicator::StateZ);
    } else if (ok) {
        led->setState(CondLedIndicator::StateOk);
    } else {
        led->setState(CondLedIndicator::StateBad);
    }
}



void MainWindow::refreshDevList() {
    clearData();
    closeModule();
    INT err = LTR_OK;
    m_modules.clear();
    ui->modulesList->clear();

    disconnect(ui->modulesList, SIGNAL(currentIndexChanged(int)), this, SLOT(moduleChanged()));
    // устанавливаем связь с управляющим каналом сервера, чтобы получить список крейтов
    TLTR srv;
    LTR_Init(&srv);
    err=LTR_OpenSvcControl(&srv, LTRD_ADDR_DEFAULT, LTRD_PORT_DEFAULT);
    if (err!=LTR_OK) {
        showError("Не удалось установить связь с сервером", err);
    } else {
        char serial_list[LTR_CRATES_MAX][LTR_CRATE_SERIAL_SIZE];
        //получаем список серийных номеров всех подключенных крейтов
        err=LTR_GetCrates(&srv, (BYTE*)&serial_list[0][0]);
        //серверное соединение больше не нужно - можно закрыть
        LTR_Close(&srv);
        if (err!=LTR_OK) {
            showError("Не удалось получить список крейтов", err);
        } else {
            for (int crate_ind=0 ; crate_ind < LTR_CRATES_MAX; crate_ind++) {
                //каждому крейту соответствует непустой серийный номер
                if (serial_list[crate_ind][0]!='\0') {
                    // Устанавливаем связь с каждым крейтом, чтобы получить список модулей
                    TLTR crate;
                    LTR_Init(&crate);
                    err =  LTR_OpenCrate(&crate, LTRD_ADDR_DEFAULT, LTRD_PORT_DEFAULT,
                                         LTR_CRATE_IFACE_UNKNOWN, serial_list[crate_ind]);
                    if (err==LTR_OK) {
                        WORD mids[LTR_MODULES_PER_CRATE_MAX];
                        err=LTR_GetCrateModules(&crate, mids);
                        if (err==LTR_OK) {
                            for (int module_ind=0; module_ind < LTR_MODULES_PER_CRATE_MAX; module_ind++) {
                                //ищем модули LTR210
                                if (mids[module_ind]==LTR_MID_LTR216) {
                                    // сохраняем информацию о найденном модуле, необходимую для
                                    // последующего установления соединения с ним, в список
                                    m_modules.append(ModuleRef(QSTRING_FROM_CSTR(serial_list[crate_ind]),
                                                               module_ind+LTR_CC_CHNUM_MODULE1));
                                    // и добавляем в ComboBox для возможности выбора нужного
                                    ui->modulesList->addItem("Крейт " + m_modules.last().csn +
                                            ", Слот " + QString::number(m_modules.last().slot));
                                }
                            }
                        }
                        //закрываем соединение с крейтом
                        LTR_Close(&crate);
                    }
                }
            }
        }
    }

    if (!m_modules.empty())
        ui->modulesList->setCurrentIndex(0);

    moduleChanged();
    connect(ui->modulesList, SIGNAL(currentIndexChanged(int)), this, SLOT(moduleChanged()));
}

void MainWindow::moduleChanged() {
    closeModule();

    int idx = ui->modulesList->currentIndex();
    if (idx >= 0) {
        INT err = LTR216_Open(&hmodule, LTRD_ADDR_DEFAULT, LTRD_PORT_DEFAULT,
                              QSTRING_TO_CSTR(m_modules.at(idx).csn), m_modules.at(idx).slot);
        if (err != LTR_OK) {
            showError("Не удалось установить связь с модулем", err);
            LTR216_Close(&hmodule);
        } else {
            ui->fpgaVer->setText(QString::number(hmodule.ModuleInfo.VerFPGA));
            ui->serial->setText(QString::fromLocal8Bit(hmodule.ModuleInfo.Serial));
            startDevice();
        }
    }
    updateUiState();
}


void MainWindow::closeModule() {
    if (running()) {
        stopDevice();
    }

    if (LTR216_IsOpened(&hmodule) == LTR_OK) {
        /* stop */
        LTR216_Close(&hmodule);
    }
}
