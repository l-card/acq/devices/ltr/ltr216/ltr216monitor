#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QThread>
#include "ltr/include/ltr216api.h"
#include "DeviceConfig.h"
#include "DataBlockProcessor.h"
#include "MesValStateWidget.h"
namespace Ui {
class MainWindow;
}

class QCPGraph;
class CondLedIndicator;
class QTableWidgetItem;
class LTR216RecvThread;


class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

protected:
    void changeEvent(QEvent *e);
    void closeEvent (QCloseEvent * event);

Q_SIGNALS:
    void procParamsUpdated(int wintype);
private Q_SLOTS:
    void updateUiState();
    void refreshDevList();
    void moduleChanged();
    void closeModule();
    void showError(QString msg, INT err);

    void configure();
    void showBlock(QSharedPointer<DataBlockProcessor::DataBlock> block);
    void clearData();

    void updateConfigUi();
    void startDevice();
    void stopDeviceReq();
    void stopDevice();
    void onRecvThreadFinished();

    void onParamTableItemChanged(QTableWidgetItem *item);
private:
    bool running();
    void addCh(QSettings &set);
    void removeCh(QSettings &set);
    void saveCh(QSettings &set, int idx);


    void setMeasTableVal(QTableWidgetItem *item, const TLTR216_MEASSTATE *state, bool ok, int prec = 3);
    void initMeasTableVal(QTableWidgetItem *item, bool enabled);
    void setMeasLedVal(CondLedIndicator *led, int status, bool ok);

    Ui::MainWindow *ui;

    struct ModuleRef {
        ModuleRef(QString _csn, WORD _slot) : csn(_csn), slot(_slot) {}

        QString csn;
        WORD slot;
    };

    struct ChUi {
        QCPGraph *timeGraph;
        QCPGraph *fftGraph;

        CondLedIndicator *ovStatus;
        CondLedIndicator *udStatus;
        CondLedIndicator *ucmStatus;
        CondLedIndicator *uxShortStatus;
        CondLedIndicator *uxOpenStatus;

        QTableWidgetItem *itemCh;
        QTableWidgetItem *itemDC;
        QTableWidgetItem *itemAC;
        QTableWidgetItem *itemUabs;
        QTableWidgetItem *itemUdiff;
        QTableWidgetItem *itemUcm;
        QTableWidgetItem *itemUcmMin;
        QTableWidgetItem *itemUcmMax;

        QTableWidgetItem *itemPeakFreq;
        QTableWidgetItem *itemPeakAmp;
        QTableWidgetItem *itemSNR;
        QTableWidgetItem *itemSINAD;
        QTableWidgetItem *itemENOB;

        QList<QTableWidgetItem *> measItemList;
    };

    QList<ChUi *> m_chUiList;




    QList<ModuleRef> m_modules;
    TLTR216 hmodule;
    bool m_stop_req;
    LTR216RecvThread *m_recvThread;
    DataBlockProcessor m_proc;
    QThread m_procThread;

    QSharedPointer<DeviceConfig> m_cfg;
};

#endif // MAINWINDOW_H
