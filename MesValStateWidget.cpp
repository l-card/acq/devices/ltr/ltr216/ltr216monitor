#include "MesValStateWidget.h"
#include "ui_MesValStateWidget.h"
#include "ltr/include/ltr216api.h"


MesValStateWidget::MesValStateWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MesValStateWidget), m_meas_en(false), m_open_en(false), m_short_en(false) {
    ui->setupUi(this);
}

MesValStateWidget::~MesValStateWidget() {
    delete ui;
}

void MesValStateWidget::setOpenVisible(bool vis) {
    ui->openLed->setVisible(vis);
    ui->openLbl->setVisible(vis);
}

void MesValStateWidget::setShortVisible(bool vis) {
    ui->shortLed->setVisible(vis);
    ui->shortLbl->setVisible(vis);
}

void MesValStateWidget::setMeasEnabled(bool en) {
    m_meas_en = en;
    ui->value->setEnabled(en);
    ui->ovLed->setState(en ? CondLedIndicator::StateOff : CondLedIndicator::StateDisabled);
}

void MesValStateWidget::setOpenEnabled(bool en) {
    m_open_en = en;
    ui->openLed->setState(en ? CondLedIndicator::StateOff : CondLedIndicator::StateDisabled);
}

void MesValStateWidget::setShortEnabled(bool en) {
    m_short_en = en;
    ui->shortLed->setState(en ? CondLedIndicator::StateOff : CondLedIndicator::StateDisabled);
}

void MesValStateWidget::setVal(TLTR216_MEASSTATE *state, bool ok, QString suffix, int prec) {
    QLocale locale;

    if ((state->Status == LTR216_MEASSTATUS_ADC_OVERRANGE) ||
            (state->Status == LTR216_MEASSTATUS_BAD_VALUE_RANGE)) {
        ui->ovLed->setState(CondLedIndicator::StateBad);
    } else if (m_meas_en) {
        if ((state->Status == LTR216_MEASSTATUS_OK) && ok) {
            ui->ovLed->setState(CondLedIndicator::StateOk);
        }  else if (state->Status == LTR216_MEASSTATUS_NOT_INIT) {
            ui->ovLed->setState(CondLedIndicator::StateOff);
        } else if ((state->Status == LTR216_MEASSTATUS_OPEN) ||
                   (state->Status == LTR216_MEASSTATUS_SHORT) ||
                   (state->Status == LTR216_MEASSTATUS_CANT_CALC)) {
            ui->ovLed->setState(CondLedIndicator::StateZ);
        } else {
            ui->ovLed->setState(CondLedIndicator::StateBad);
        }
    }
    ui->value->setEnabled(state->Status != LTR216_MEASSTATUS_NOT_USED);
    if (state->ValueValid) {
        ui->value->setText(locale.toString(state->Value, 'f', prec) + suffix);
    } else {
        ui->value->setText("");
    }

    if (state->Status != LTR216_MEASSTATUS_NOT_INIT) {
        ui->openLed->setState(state->Status == LTR216_MEASSTATUS_OPEN ? CondLedIndicator::StateBad :
                                                     !m_open_en ? CondLedIndicator::StateDisabled :
                               state->Status == LTR216_MEASSTATUS_CANT_CALC ? CondLedIndicator::StateZ
                                                                            : CondLedIndicator::StateOk);


        ui->shortLed->setState(state->Status == LTR216_MEASSTATUS_SHORT ? CondLedIndicator::StateBad :
                                        !m_short_en ? CondLedIndicator::StateDisabled :
                               state->Status == LTR216_MEASSTATUS_OPEN ? CondLedIndicator::StateZ :
                               state->Status == LTR216_MEASSTATUS_CANT_CALC ? CondLedIndicator::StateZ :
                                                                              CondLedIndicator::StateOk);
    }
}

void MesValStateWidget::clear() {
    ui->value->setText("");
}

void MesValStateWidget::changeEvent(QEvent *e) {
    QWidget::changeEvent(e);
    switch (e->type()) {
        case QEvent::LanguageChange:
            ui->retranslateUi(this);
            break;
        default:
            break;
    }
}
