#ifndef MESVALSTATEWIDGET_H
#define MESVALSTATEWIDGET_H

#include <QWidget>
#include "ltr/include/ltr216api.h"

namespace Ui {
class MesValStateWidget;
}

class MesValStateWidget : public QWidget {
    Q_OBJECT

public:
    explicit MesValStateWidget(QWidget *parent = 0);
    ~MesValStateWidget();

    void setOpenVisible(bool vis);
    void setShortVisible(bool vis);

    void setMeasEnabled(bool en);
    void setOpenEnabled(bool en);
    void setShortEnabled(bool en);



    void setVal(TLTR216_MEASSTATE *state, bool ok, QString suffix = QString(), int prec = 4);
    void clear();
protected:
    void changeEvent(QEvent *e);

private:
    Ui::MesValStateWidget *ui;
    bool m_open_en, m_short_en, m_meas_en;
};

#endif // MESVALSTATEWIDGET_H
