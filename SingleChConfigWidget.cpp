#include "SingleChConfigWidget.h"
#include "ui_SingleChConfigWidget.h"
#include "DeviceConfig.h"

SingleChConfigWidget::SingleChConfigWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SingleChConfigWidget), m_cfgUpdated(false) {
    ui->setupUi(this);

    for (unsigned ch=0; ch < LTR216_CHANNELS_CNT; ch++) {
        ui->channelBox->addItem(QString::number(ch+1), ch);
    }

    addRangeItem(ui->rangeBox, 35, LTR216_RANGE_35);
    addRangeItem(ui->rangeBox, 70, LTR216_RANGE_70);
}

SingleChConfigWidget::~SingleChConfigWidget() {
    delete ui;
}

void SingleChConfigWidget::setConfig(DeviceConfig *cfg) {
    m_cfg = cfg;
    connect(ui->channelBox, SIGNAL(currentIndexChanged(int)), SLOT(updateConfig()));
    connect(ui->rangeBox, SIGNAL(currentIndexChanged(int)), SLOT(updateConfig()));
}

void SingleChConfigWidget::showConfig() {
    m_cfgUpdated = true;
    for (int ch = 0; ch < LTR216_CHANNELS_CNT; ch++) {
        if (m_cfg->channelEnabled(ch)) {
            setComboBoxItemByData(ui->channelBox, ch);
            setComboBoxItemByData(ui->rangeBox, m_cfg->channelRange(ch));
            break;
        }
    }
    m_cfgUpdated = false;
}

void SingleChConfigWidget::updateConfig() {
    if (!m_cfgUpdated) {
        int en_ch_num = ui->channelBox->currentData().toInt();
        int range = ui->rangeBox->currentData().toInt();
        for (int ch = 0; ch < LTR216_CHANNELS_CNT; ch++) {
            m_cfg->setChannelEnabled(ch, ch == en_ch_num);
            m_cfg->setChannelRange(ch, range);
        }
    }
}

void SingleChConfigWidget::changeEvent(QEvent *e) {
    QWidget::changeEvent(e);
    switch (e->type()) {
        case QEvent::LanguageChange:
            ui->retranslateUi(this);
            break;
        default:
            break;
    }
}

void SingleChConfigWidget::addRangeItem(QComboBox *box, double val, unsigned code) {
    box->addItem("± " + QString::number(val) + " мВ",  code);
}

void SingleChConfigWidget::setComboBoxItemByData(QComboBox *box, int data) {
    int idx = 0;
    for (int i=0; i < box->count(); i++) {
        if (box->itemData(i).toInt()==data) {
            idx = i;
            break;
        }
    }
    box->setCurrentIndex(idx);
}
