#ifndef SINGLECHCONFIGWIDGET_H
#define SINGLECHCONFIGWIDGET_H

#include <QWidget>
class QComboBox;
class DeviceConfig;

namespace Ui {
class SingleChConfigWidget;
}

class SingleChConfigWidget : public QWidget {
    Q_OBJECT
public:
    explicit SingleChConfigWidget(QWidget *parent = 0);
    ~SingleChConfigWidget();

    void setConfig(DeviceConfig *cfg);
public Q_SLOTS:
    void showConfig();
    void updateConfig();
protected:
    void changeEvent(QEvent *e);
private:
    void addRangeItem(QComboBox *box, double val, unsigned code);
    void setComboBoxItemByData(QComboBox *box, int data);


    Ui::SingleChConfigWidget *ui;
    DeviceConfig *m_cfg;
    bool m_cfgUpdated;

};

#endif // SINGLECHCONFIGWIDGET_H
