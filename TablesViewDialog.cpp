#include "TablesViewDialog.h"
#include "ui_TablesViewDialog.h"
#include <QTableWidgetItem>
#include "lib/lbitfield/lbitfield.h"
#include "CondLedIndicator.h"
#include <QSettings>

#define LCH_MSK_OFFSET   (0x3FFFUL << 0)
#define LCH_MSK_BOUT     (   0x1UL << 14)
#define LCH_MSK_SEC_TBL  (   0x1UL << 15)
#define LCH_MSK_GAIN     (   0x7UL << 16)
#define LCH_MSK_CHAN     (   0xFUL << 19)
#define LCH_MSK_SW       ( 0x7FFUL << 23)


#define  LTABLE_SW_WORD(sw1, sw2, sw3) ((((sw1) & 0xF) << 28) | \
    (((sw2) & 0xF) << 24) | \
    (((sw3) & 1) << 23))

#define LTABLE_GET_SW(word) (word & (0x1FFUL << 23))
#define LTABLE_GET_GAIN(word) (LBITFIELD_GET(LCH_MSK_GAIN, word))
#define LTABLE_GET_DAC(word) (LBITFIELD_GET(LCH_MSK_OFFSET, word))
#define LTABLE_GET_CH(word) (LBITFIELD_GET(LCH_MSK_CHAN, word))
#define LTABLE_GET_BOUT(word) (LBITFIELD_GET(LCH_MSK_BOUT, word))

#define LTABLE_SW_MAIN          LTABLE_SW_WORD(1,0,1)
#define LTABLE_SW_OFFS          LTABLE_SW_WORD(1,1,0)
#define LTABLE_SW_UREF          LTABLE_SW_WORD(4,1,0)
#define LTABLE_SW_UREF_AGND     LTABLE_SW_WORD(2,1,0)
#define LTABLE_SW_UREF_OFFS     LTABLE_SW_WORD(4,2,0)
#define LTABLE_SW_U             LTABLE_SW_WORD(4,0,1)
#define LTABLE_SW_U_AGND        LTABLE_SW_WORD(2,0,1)
#define LTABLE_SW_VADJ          LTABLE_SW_WORD(2,4,0)
#define LTABLE_SW_UNEG          LTABLE_SW_WORD(2,2,0)


static const struct {
    BYTE ku;
    double full_range;
    double user_range;
    int    range;
} f_gains[] = {
    {128,   9.77/1000,      9./1000,  -1},
    {64,   19.5/1000,      18./1000,  -1},
    {32,   39.0625/1000,   35./1000,  LTR216_RANGE_35},
    {16,   78.125/1000,    70./1000,  LTR216_RANGE_70},
    {8,   156./1000,      150./1000, -1},
    {4,   312./1000,      300./1000, -1},
    {2,   625./1000,      600./1000, -1},
    {1,  1250./1000,     1200./1000,  2}
};

static double conv_dac_val(TLTR216 *hnd, int gain_idx, DWORD dac_code) {
    double dac_range = 2.5/f_gains[gain_idx].ku;
    /** @todo Учет калибровочных коэффициентов */

    return 2. * dac_range * dac_code / 16384 - dac_range;
}

TablesViewDialog::TablesViewDialog(QWidget *parent, TLTR216 *pmodule) :
    QDialog(parent),
    ui(new Ui::TablesViewDialog) {
    ui->setupUi(this);

    QSettings set;
    set.beginGroup("TablesViewDialog");
    resize(set.value("DialogSize", QSize(850, 600)).toSize());
    set.endGroup();

    initTable(ui->mainTableWidget);
    initTable(ui->bgTableWidget);

    for (DWORD lch = 0; lch < pmodule->RawCfg.MainLChCnt; lch++) {
        QBrush bgBrush;
        if (lch >= pmodule->RawCfg.ProcChCnt) {
            bgBrush = Qt::darkGray;
        } else if (lch >= pmodule->RawCfg.UserChCnt) {
            bgBrush = Qt::lightGray;
        }

        showChannel(pmodule, ui->mainTableWidget, lch, pmodule->RawCfg.MainLChTbl[lch],
                    true, bgBrush);
    }

    for (DWORD lch = 0; lch < pmodule->RawCfg.BgLChCnt; lch++) {
        showChannel(pmodule, ui->bgTableWidget, lch, pmodule->RawCfg.BgLChTbl[lch]);
    }

    ui->mainTableWidget->resizeColumnsToContents();
    ui->bgTableWidget->resizeColumnsToContents();
}

TablesViewDialog::~TablesViewDialog() {
    QSettings set;
    set.beginGroup("TablesViewDialog");
    set.setValue("DialogSize", size());
    set.endGroup();

    delete ui;
}

void TablesViewDialog::changeEvent(QEvent *e) {
    QDialog::changeEvent(e);
    switch (e->type()) {
        case QEvent::LanguageChange:
            ui->retranslateUi(this);
            break;
        default:
            break;
    }
}

void TablesViewDialog::initTable(QTableWidget *wgt) {
    QStringList headers;
    headers << tr("Режим") << tr("Диапазон") << tr("Канал") << tr("ЦАП") << tr("Bout");
    wgt->setColumnCount(headers.size());
    wgt->setHorizontalHeaderLabels(headers);
    wgt->setEditTriggers(QAbstractItemView::NoEditTriggers);
    wgt->horizontalHeader()->setSectionResizeMode(0, QHeaderView::Stretch);
}

void TablesViewDialog::showChannel(TLTR216 *pmodule, QTableWidget *wgt, int row, DWORD lch_wrd, bool dac_prec, QBrush bgBrush) {
    wgt->setRowCount(row+1);
    bool bg_table = lch_wrd & LCH_MSK_SEC_TBL;
    if (bg_table) {
        QTableWidgetItem *item = new QTableWidgetItem(tr("Фоновая таблица"));
        item->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
        item->setBackground(Qt::lightGray);
        wgt->setItem(row, 0, item);
        wgt->setSpan(row, 0, 1, wgt->columnCount());
    } else {
        DWORD sw_code = lch_wrd & LCH_MSK_SW;
        DWORD gain = LTABLE_GET_GAIN(lch_wrd);
        DWORD ch = LTABLE_GET_CH(lch_wrd);
        DWORD dac_code = LTABLE_GET_DAC(lch_wrd);
        bool bout = LTABLE_GET_BOUT(lch_wrd);

        bool has_channel = false;
        QString mode;
        if (sw_code == LTABLE_SW_MAIN) {
            mode = tr("Разбаланс");
            has_channel = true;
        } else if (sw_code == LTABLE_SW_OFFS) {
            mode = tr("Ноль (Uref)");
        } else if (sw_code == LTABLE_SW_UREF) {
            mode = tr("Uref (U-)");
        } else if (sw_code == LTABLE_SW_UREF_AGND) {
            mode = tr("Uref (AGND)");
        } else if (sw_code == LTABLE_SW_UREF_OFFS) {
            mode = tr("Ноль (U-)");
        } else if (sw_code == LTABLE_SW_U) {
            mode = tr("Ux (U-)");
            has_channel = true;
        } else if (sw_code == LTABLE_SW_U_AGND) {
            mode = tr("Ux (AGND)");
            has_channel = true;
        } else if (sw_code == LTABLE_SW_VADJ) {
            mode = tr("Vadj");
        } else if (sw_code == LTABLE_SW_UNEG) {
            mode = tr("U- (AGND)");
        }
        mode += QString(" (%1,%2,%3)").arg((sw_code >> 28) & 0xF)
                .arg((sw_code >> 24) & 0xF)
                .arg((sw_code >> 23) & 1);

        QTableWidgetItem *item = new QTableWidgetItem(mode);
        item->setBackground(bgBrush);
        wgt->setItem(row, 0, item);

        item = new QTableWidgetItem(tr("± %1 мВ").arg((int)(f_gains[gain].user_range * 1000)));
        item->setBackground(bgBrush);
        wgt->setItem(row, 1, item);

        item = new QTableWidgetItem(has_channel ? QString::number(ch + 1) : "");
        item->setBackground(bgBrush);
        wgt->setItem(row, 2, item);


        double dac_val = conv_dac_val(pmodule, gain, dac_code) * 1000;
        item = new QTableWidgetItem(tr("%1 мВ").arg(dac_val, 0, 'f', dac_prec ? 3 : 0));
        item->setBackground(bgBrush);
        wgt->setItem(row, 3, item);

        item = new QTableWidgetItem();
        item->setBackground(bgBrush);
        wgt->setItem(row, 4, item);
        CondLedIndicator *boutLed = new CondLedIndicator();
        boutLed->setState(bout ? CondLedIndicator::StateBad : CondLedIndicator::StateDisabled);
        wgt->setCellWidget(row, 4, boutLed);


    }
}
