#ifndef TABLESVIEWDIALOG_H
#define TABLESVIEWDIALOG_H

#include <QDialog>
#include "ltr/include/ltr216api.h"

namespace Ui {
class TablesViewDialog;
}
class QTableWidget;

class TablesViewDialog : public QDialog {
    Q_OBJECT
public:
    explicit TablesViewDialog(QWidget *parent, TLTR216 *pmodule);
    ~TablesViewDialog();

protected:
    void changeEvent(QEvent *e);

private:
    void initTable(QTableWidget *wgt);
    void showChannel(TLTR216 *pmodule, QTableWidget *wgt, int row, DWORD lch, bool dac_prec = false, QBrush bgBrush = QBrush());

    Ui::TablesViewDialog *ui;
};

#endif // TABLESVIEWDIALOG_H
