#include "TareChannelsDialog.h"
#include "ui_TareChannelsDialog.h"
#include "ComboBoxNoWheel.h"
#include <QSettings>

TareChannelsDialog::TareChannelsDialog(QWidget *parent, DeviceConfig *cfg) :
    QDialog(parent),
    ui(new Ui::TareChannelsDialog) {
    ui->setupUi(this);


    QSettings set;
    set.beginGroup("TareChannelsDialog");
    resize(set.value("WidgetSize", QSize(400, 600)).toSize());
    set.endGroup();

    ui->channelsCfg->setConfig(cfg);
    ui->channelsCfg->showConfig();
}

TareChannelsDialog::~TareChannelsDialog() {
    QSettings set;
    set.beginGroup("TareChannelsDialog");
    set.setValue("WidgetSize", size());
    set.endGroup();
    delete ui;
}

void TareChannelsDialog::changeEvent(QEvent *e) {
    QDialog::changeEvent(e);
    switch (e->type()) {
        case QEvent::LanguageChange:
            ui->retranslateUi(this);
            break;
        default:
            break;
    }
}
