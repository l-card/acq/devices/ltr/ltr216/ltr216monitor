#ifndef TARECHANNELSDIALOG_H
#define TARECHANNELSDIALOG_H

#include <QDialog>
#include "ltr/include/ltr216api.h"
class QComboBox;
class QTableWidgetItem;
class QTableWidget;
class DeviceConfig;


namespace Ui {
class TareChannelsDialog;
}

class TareChannelsDialog : public QDialog {
    Q_OBJECT

public:
    explicit TareChannelsDialog(QWidget *parent, DeviceConfig *cfg);
    ~TareChannelsDialog();

protected:
    void changeEvent(QEvent *e);

private:
    Ui::TareChannelsDialog *ui;
};

#endif // TARECHANNELSDIALOG_H
