#include "TareCoefsModel.h"
#include "TareCoefsModelParams.h"

TareCoefsModel::TareCoefsModel(TLTR216 *pmodule) {
    paramAdd(new TareCoefsModelParamChName());
    paramAdd(new TareCoefsModelParamDacValue());
    paramAdd(new TareCoefsModelParamOffset());
    paramAdd(new TareCoefsModelParamScale());

    for (int ch = 0; ch < LTR216_CHANNELS_CNT; ch++) {
        paramItemAdd(new TareChParam(ch, &pmodule->ModuleInfo.Tare[ch]));
    }
}

void TareCoefsModel::initView(QTableView *view, QString viewName, QSettings &set) {
    view->setModel(this);
    paramInitView(view);

    TableViewSettings::instance()->initDataView(view, viewName, set, TableViewSettings::InitNoAction);
}

void TareCoefsModel::updateData() {
    Q_EMIT dataChanged(createIndex(0, 0), createIndex(paramItemsCount()-1, paramCount() - 1));
}
