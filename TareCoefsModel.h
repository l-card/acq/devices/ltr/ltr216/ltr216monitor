#ifndef TARECOEFSMODEL_H
#define TARECOEFSMODEL_H

#include <QAbstractTableModel>
#include "TableParamsModelHelper.h"
#include "ltr/include/ltr216api.h"

struct TareChParam {
    TareChParam(int init_ch_num, TLTR216_TARE_CH_COEFS *init_pcoef) :
        ch_num(init_ch_num), pcoefs(init_pcoef) {}

    int ch_num;
    TLTR216_TARE_CH_COEFS *pcoefs;
};


class TareCoefsModel : public QAbstractTableModel, public TableParamsModelHelper<TareChParam>,
        public TableParamsModelIface {
    Q_OBJECT
public:
    TABLE_PARAM_MODEL_METHODS

    TareCoefsModel(TLTR216 *pmodule);

    void initView(QTableView *view, QString viewName, QSettings &set);

    void updateData();

};

#endif // TARECOEFSMODEL_H
