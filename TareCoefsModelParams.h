#ifndef TARECOEFSMODELPARAMS_H
#define TARECOEFSMODELPARAMS_H

#include "TareCoefsModel.h"

class TareCoefsModelParamChName : public TareCoefsModel::Param {
public:

    QString name() const {return TareCoefsModel::tr("Канал");}
    bool readOnly(TareChParam *tare) const {return true;}
    bool customReadonlyColor(TareChParam *tare) const {return true;}
    unsigned columnWidth() const {return 120;}

    QVariant protData(TareChParam *tare, int role) const {
        QVariant ret;
        if (role == Qt::DisplayRole) {
            ret = QString("Канал %1").arg(tare->ch_num + 1);
        }
        return ret;
    }
};


class TareCoefsModelParamDacValue: public TareCoefsModel::Param {
public:

    QString name() const {return TareCoefsModel::tr("Смещение ЦАП (мВ)");}

    unsigned columnWidth() const {return 150;}

    QVariant protData(TareChParam *tare, int role) const {
        QVariant ret;
        if ((role == Qt::DisplayRole) || (role == Qt::EditRole)) {
            ret = tare->pcoefs->DacValue * 1000;
        } else if (role == Qt::CheckStateRole) {
            ret = tare->pcoefs->DacValid ? Qt::Checked : Qt::Unchecked;
        }
        return ret;
    }

    bool protSetData(TareChParam *tare, const QVariant &value, int role) const {
        bool ok = false;
        if (role == Qt::EditRole) {
            tare->pcoefs->DacValue = value.toDouble()/1000;
            ok = true;
        } else if (role == Qt::CheckStateRole) {
            bool checked = value.toInt() == Qt::Checked;
            tare->pcoefs->DacValid = checked;
            ok = true;
        }
        return ok;
    }

    Qt::ItemFlags flags(TareChParam *tare) const {
        return TareCoefsModel::Param::flags(tare) | Qt::ItemIsUserCheckable;
    }
};


class TareCoefsModelParamOffset: public TareCoefsModel::Param {
public:

    QString name() const {return TareCoefsModel::tr("Коэф. смещения (мВ)");}

    unsigned columnWidth() const {return 150;}

    QVariant protData(TareChParam *tare, int role) const {
        QVariant ret;
        if ((role == Qt::DisplayRole) || (role == Qt::EditRole)) {
            ret = tare->pcoefs->Offset * 1000;
        } else if (role == Qt::CheckStateRole) {
            ret = tare->pcoefs->OffsetValid ? Qt::Checked : Qt::Unchecked;
        }
        return ret;
    }

    bool protSetData(TareChParam *tare, const QVariant &value, int role) const {
        bool ok = false;
        if (role == Qt::EditRole) {
            tare->pcoefs->Offset = value.toDouble()/1000;
            ok = true;
        } else if (role == Qt::CheckStateRole) {
            bool checked = value.toInt() == Qt::Checked;
            tare->pcoefs->OffsetValid = checked;
            ok = true;
        }
        return ok;
    }

    Qt::ItemFlags flags(TareChParam *tare) const {
        return TareCoefsModel::Param::flags(tare) | Qt::ItemIsUserCheckable;
    }
};

class TareCoefsModelParamScale: public TareCoefsModel::Param {
public:
    QString name() const {return TareCoefsModel::tr("Коэф. шкалы");}
    unsigned columnWidth() const {return 150;}

    QVariant protData(TareChParam *tare, int role) const {
        QVariant ret;
        if ((role == Qt::DisplayRole) || (role == Qt::EditRole)) {
            ret = tare->pcoefs->Scale;
        } else if (role == Qt::CheckStateRole) {
            ret = tare->pcoefs->ScaleValid ? Qt::Checked : Qt::Unchecked;
        }
        return ret;
    }

    bool protSetData(TareChParam *tare, const QVariant &value, int role) const {
        bool ok = false;
        if (role == Qt::EditRole) {
            tare->pcoefs->Scale = value.toDouble();
            ok = true;
        } else if (role == Qt::CheckStateRole) {
            bool checked = value.toInt() == Qt::Checked;
            tare->pcoefs->ScaleValid = checked;
            ok = true;
        }
        return ok;
    }

    Qt::ItemFlags flags(TareChParam *tare) const {
        return TareCoefsModel::Param::flags(tare) | Qt::ItemIsUserCheckable;
    }
};


#endif // TARECOEFSMODELPARAMS_H
