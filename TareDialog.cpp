#include "TareDialog.h"
#include "ui_TareDialog.h"
#include <QSettings>
#include <QMessageBox>
#include "TareChannelsDialog.h"
#include "lqtdefs.h"

TareDialog::TareDialog(QWidget *parent, TLTR216 *pmodule, DeviceConfig *defCfg) :
    QDialog(parent),
    ui(new Ui::TareDialog),
    m_cfg(new DeviceConfig(*defCfg)),
    m_pmodule(pmodule), m_coefModel(pmodule) {

    ui->setupUi(this);


    QSettings set;
    set.beginGroup("TareDialog");
    resize(set.value("WidgetSize", QSize(1000, 600)).toSize());

    m_coefModel.initView(ui->tareCoefTable, "CoefTable", set);

    set.endGroup();


    connect(ui->tareWriteFlashBtn, SIGNAL(clicked(bool)), SLOT(writeCoefs()));
    connect(ui->tareReadFlashBtn, SIGNAL(clicked(bool)), SLOT(readCoefs()));
    connect(ui->tareOffsBtn, SIGNAL(clicked(bool)), SLOT(tareOffset()));
    connect(ui->tareScaleBtn, SIGNAL(clicked(bool)), SLOT(tareScale()));
}

TareDialog::~TareDialog() {
    QSettings set;
    set.beginGroup("TareDialog");
    set.setValue("WidgetSize", size());
    TableViewSettings::instance()->saveDataView(ui->tareCoefTable, set);
    set.endGroup();

    delete ui;
}

void TareDialog::showError(QString msg, INT err) {
    QMessageBox::critical(this, "Ошибка", QString("%1. Ошибка %2: %3").arg(msg).arg(err).arg(QSTRING_FROM_CSTR(LTR216_GetErrorString(err))));
}

void TareDialog::writeCoefs() {
    INT err = LTR216_WriteTareInfo(m_pmodule, LTR216_CHANNEL_MASK_ALL);
    if (err == LTR_OK) {
        QMessageBox::information(this, tr("Завершение операции"), tr("Запись тарировочных коэффициентов выполнена успешно"));
    } else {
        showError(tr("Не удалось записать тарировочные коэффициенты"), err);
    }
}

void TareDialog::readCoefs() {
    INT err = LTR216_ReadTareInfo(m_pmodule, LTR216_CHANNEL_MASK_ALL);
    if (err == LTR_OK) {
        m_coefModel.updateData();
        QMessageBox::information(this, tr("Завершение операции"), tr("Чтение тарировочных коэффициентов выполнено успешно"));
    } else {
        showError(tr("Не удалось прочитать тарировочные коэффициенты"), err);
    }
}

void TareDialog::tareOffset() {
    TareChannelsDialog dlg(this, m_cfg.data());
    if (dlg.exec() == QDialog::Accepted) {
        DWORD ok_ch;
        m_cfg->fillDeviceConfig(m_pmodule);
        INT err = LTR216_TareOffset(m_pmodule, LTR216_CHANNEL_MASK_ALL, &ok_ch);
        showTareResult(err, ok_ch);
    }
}

void TareDialog::tareScale() {
    TareChannelsDialog dlg(this, m_cfg.data());
    if (dlg.exec() == QDialog::Accepted) {
        DWORD ok_ch;
        m_cfg->fillDeviceConfig(m_pmodule);
        INT err = LTR216_InitMeasParams(m_pmodule, LTR216_INIT_MEAS_UREF, 0, NULL);
        if (err == LTR_OK)
            err = LTR216_TareScale(m_pmodule, LTR216_CHANNEL_MASK_ALL, &ok_ch);
        showTareResult(err, ok_ch);

    }
}

void TareDialog::changeEvent(QEvent *e) {
    QDialog::changeEvent(e);
    switch (e->type()) {
        case QEvent::LanguageChange:
            ui->retranslateUi(this);
            break;
        default:
            break;
    }
}

void TareDialog::showTareResult(INT err, DWORD ch_ok_msk) {
    if (err == LTR_OK) {
        m_coefModel.updateData();
        QMessageBox::information(this, tr("Завершение операции"), tr("Тарировка выполнена успешно"));
    } else if (err == LTR216_ERR_TARE_CHANNELS) {
        if (ch_ok_msk == 0) {
            QMessageBox::critical(this, tr("Ошибка тарировки"), tr("Не удалось найти действительное значение ни для одного канала"));
        } else {
            QString ok_chs_str;
            for (int ch = 0; ch < LTR216_CHANNELS_CNT; ch++) {
                if (ch_ok_msk & (1 << ch)) {
                    if (!ok_chs_str.isEmpty()) {
                        ok_chs_str = ok_chs_str.append(", ");
                    }
                    ok_chs_str = ok_chs_str.append(QString::number(ch+1));
                }
            }
            QMessageBox::critical(this, tr("Ошибка тарировки"), tr("Не удалось найти действительное значение для всех каналов.\nТарировка выполнена успешно для каналов %1").arg(ok_chs_str));
        }

    } else {
        showError(tr("Не удалось выполнить тарировку"), err);
    }
}
