#ifndef TAREDIALOG_H
#define TAREDIALOG_H

#include <QDialog>
#include "TareCoefsModel.h"
#include "DeviceConfig.h"

namespace Ui {
class TareDialog;
}

class TareDialog : public QDialog {
    Q_OBJECT

public:
    explicit TareDialog(QWidget *parent, TLTR216 *pmodule, DeviceConfig *defCfg);
    ~TareDialog();

private Q_SLOTS:
    void showError(QString msg, INT err);
    void writeCoefs();
    void readCoefs();
    void tareOffset();
    void tareScale();
protected:
    void changeEvent(QEvent *e);

private:
    void showTareResult(INT err, DWORD ch_ok_msk);

    Ui::TareDialog *ui;
    TLTR216 *m_pmodule;
    TareCoefsModel m_coefModel;
    QSharedPointer<DeviceConfig> m_cfg;
};

#endif // TAREDIALOG_H
