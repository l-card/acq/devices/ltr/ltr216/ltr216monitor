#include <QApplication>
#include "MainWindow.h"
#include "version.h"
#include "QtTranslationSwitch.h"
#ifdef Q_OS_WIN
    #include <QSettings>
#endif

int main(int argc, char *argv[]) {
#ifdef Q_OS_WIN
    /* на Windows используем ini-файлы, а не реестр (как по умолчанию) */
    QSettings::setDefaultFormat(QSettings::IniFormat);
#endif
    /* Приложение ltrmanager должно быть запущено всегда одно.
     * уникальность определяем по сгенерированному GUID */
    QApplication a(argc, argv);

    a.setOrganizationName(PRJ_VENDOR);
    a.setApplicationName(PRJ_APP_TITLE);
    a.setOrganizationDomain("lcard.ru");
    a.setApplicationVersion(PRJ_VERSION);

    QtTranslationSwitch *ts = QtTranslationSwitch::instance();
    ts->init(PRJ_TRANSLATIONS_DIR);
    ts->setLanguage(QtTranslationSwitch::langNameRu());

    MainWindow w;

    w.show();

    return a.exec();
}
