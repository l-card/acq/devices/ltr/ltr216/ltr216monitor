# пути к внешним библиотекам по-умолчанию для ОС Windows
if(NOT PROGRAM_DIR)
    set(PROGRAM_DIR "c:/Program Files")
endif(NOT PROGRAM_DIR)

if(NOT PROGRAM_DIR32)
    set(PROGRAM_DIR32 "c:/Program Files (x86)")
endif(NOT PROGRAM_DIR32)

if(NOT EXTERN_LIB_DIR)
    set(EXTERN_LIB_DIR ${PROGRAM_DIR32})
endif(NOT EXTERN_LIB_DIR)


if(NOT NSIS_EXEC)
    set(NSIS_EXEC "${PROGRAM_DIR32}/NSIS/makensis")
endif(NOT NSIS_EXEC)

if(NOT MSVC_RT_REDISTR_DIR)
    set(MSVC_RT_REDISTR_DIR "${PROGRAM_DIR32}/Microsoft Visual Studio 14.0/VC/redist/x86/Microsoft.VC140.CRT")
endif(NOT MSVC_RT_REDISTR_DIR)

if(NOT UCRT_REDISTR)
    set(UCRT_REDISTR "${PROGRAM_DIR32}/Windows Kits/10/Redist/ucrt/DLLs/x86")
endif(NOT UCRT_REDISTR)

if(NOT WINKIT_DIR)
    set(WINKIT_DIR "${PROGRAM_DIR32}/Windows Kits/10")
endif(NOT WINKIT_DIR)

if(NOT SIGNTOOL)
    set(SIGNTOOL ${WINKIT_DIR}/bin/x64/SignTool.exe)
endif(NOT SIGNTOOL)


if(NOT SIGN_CERT)
    set(SIGN_CERT c:/PRJ/MSCV-VSClass3.cer)
endif(NOT SIGN_CERT)

if(NOT SIGN_COMPANY_NAME)
    set(SIGN_COMPANY_NAME "L Card, OOO")
endif(NOT SIGN_COMPANY_NAME)

if(NOT CERT_TIMESTAMP_URL)
    set(CERT_TIMESTAMP_URL http://timestamp.verisign.com/scripts/timstamp.dll)
endif(NOT CERT_TIMESTAMP_URL)

if(NOT LTRDLL_DIR)
    set(LTRDLL_DIR "${EXTERN_LIB_DIR}/L-Card/ltr/bin/x86")
endif(NOT LTRDLL_DIR)

if(NOT FFTW_DLL)
    set(FFTW_DLL  "${EXTERN_LIB_DIR}/libfftw3/bin/x86/libfftw3-3.dll")
endif(NOT FFTW_DLL)
